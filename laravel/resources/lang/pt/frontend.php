<?php

return [

    // Header NAV 
    'header' => [
        'home'             => 'HOME',
        'a-abint'          => 'A ABINT',
        'associados'       => 'ASSOCIADOS',
        'naotecidos'       => 'NÃOTECIDOS',
        'texteis-tecnicos' => 'TÊXTEIS TÉCNICOS',
        'informacao'       => 'INFORMAÇÃO',
        'publicacoes'      => 'Publicações',
        'cursos-eventos'   => 'Cursos e Eventos',
        'normas-ensaios'   => 'Normas e Ensaios',
        'imprensa'         => 'Imprensa',
        'outros'           => 'Outros',
        'associe-se'       => 'ASSOCIE-SE',
        'contato'          => 'CONTATO',
        'buscar'           => 'buscar...',
    ],

    // HOME
    'home' => [
        'titulo-objetivos' => 'OBJETIVOS',
        'objetivos'        => 'Objetivos da ABINT',
        'busca-objetivos'  => 'Clique aqui e saiba mais sobre os objetivos da ABINT...',
        'saiba-mais'       => 'SAIBA MAIS SOBRE A ABINT ',
        'naotecidos'       => 'nãotecidos',
        'tecidos-tecnicos' => 'tecidos técnicos',
        'publicacoes'      => 'publicações',
        'mais-informacoes' => 'mais informações',
    ],

    // A ABINT
    'abint' => [
        'titulo'         => 'A ABINT',
        'titulo-busca'   => 'Sobre a ABINT',
        'diretoria'      => 'Diretoria',
        'presidente'     => 'Presidente:',
        'vice'           => 'Vice-Presidente:',
        'conselheiros'   => 'Conselheiros:',
        'diretores'      => 'Diretores:',
        'titulo-comites' => 'COMITÊS',
        'comites'        => 'Comitês técnicos',
        'busca-comites'  => "Clique aqui e saiba mais sobre este comitê técnico...",
        'empresa'        => 'Empresa: ',
        'busca-abint'    => '. Clique aqui e saiba mais sobre a diretoria da ABINT...',
    ],

    // ASSOCIADOS
    'associados' => [
        'titulo'           => 'ASSOCIADOS',
        'maquinas'         => 'Máquinas e Equipamentos',
        'mat-primas'       => 'Matérias primas e Insumos',
        'fab-naotecidos'   => 'Fabricantes de Nãotecidos',
        'fab-texteis-tec'  => 'Fabricantes de Tecidos e/ou Têxteis Técnicos',
        'convertedores'    => 'Convertedores e Distribuidores',
        'busca'            => ". Clique aqui e veja todos os associados...",
        'busca-grupo'      => "Clique aqui e veja todos os associados deste grupo...",
        'link-texto'       => "CONHEÇA TODAS AS EMPRESAS ASSOCIADAS ABINT",
    ],

    // NAOTECIDOS
    'naotecidos' => [
        'titulo'                => 'NÃOTECIDOS',
        'o-que-sao'             => 'O que são',
        'titulo-o-que-sao'      => 'O QUE SÃO OS NÃOTECIDOS',
        'historia'              => 'História',
        'titulo-historia'       => 'A HISTÓRIA DOS NÃOTECIDOS',
        'mat-primas'            => 'Matérias-primas',
        'titulo-mat-primas'     => 'MATÉRIAS-PRIMAS UTILIZADAS',
        'fabricacao'            => 'Como são fabricados',
        'titulo-fabricacao'     => 'COMO SÃO FABRICADOS OS NÃOTECIDOS',
        'transformacao'         => 'Transformação, acabamento e/ou conversão',
        'titulo-transformacao'  => 'TRANSFORMAÇÃO, ACABAMENTO E/OU CONVERSÃO DOS NÃOTECIDOS',
        'classificacao'         => 'Classificação',
        'titulo-classificacao'  => 'CLASSIFICAÇÃO DOS NÃOTECIDOS',
        'identificacao'         => 'Identificação',
        'titulo-identificacao'  => 'IDENTIFICAÇÃO DOS NÃOTECIDOS',
        'algumas-aplicacoes'    => 'Algumas aplicações',
        'titulo-aplicacoes'     => 'ALGUMAS APLICAÇÕES',
        'aplicacoes-a-z'        => 'Aplicações de A a Z',
        'titulo-aplicacoes-a-z' => 'APLICAÇÕES DE A - Z',
        'frase-aplicacoes-a-z'  => 'Clique nas letras e conheça alguns produtos onde são empregados os Nãotecidos.',
        'busca'                 => "Informações sobre os nãotecidos",
    ],

    // TEXTEIS TECNICOS
    'texteis-tec' => [
        'titulo'               => 'TÊXTEIS TÉCNICOS',
        'o-que-sao'            => 'O que são',
        'titulo-o-que-sao'     => 'O QUE SÃO OS TÊXTEIS TÉCNICOS',
        'mat-primas'           => 'Matérias-primas',
        'titulo-mat-primas'    => 'MATÉRIAS-PRIMAS UTILIZADAS',
        'fabricacao'           => 'Como são fabricados',
        'titulo-fabricacao'    => 'COMO SÃO FABRICADOS OS TÊXTEIS TÉCNICOS',
        'processos'            => 'Processos de transformação e acabamento',
        'titulo-processos'     => 'PROCESSOS DE TRANSFORMAÇÃO E ACABAMENTO',
        'classificacao'        => 'Classificação',
        'titulo-classificacao' => 'CLASSIFICAÇÃO DOS TÊXTEIS TÉCNICOS',
        'desempenho'           => 'Desempenho',
        'titulo-desempenho'    => 'DETERMINANTES DO DESEMPENHO DOS TÊXTEIS TÉCNICOS',
        'identificacao'        => 'Identificação',
        'titulo-identificacao' => 'IDENTIFICAÇÃO DOS TÊXTEIS TÉCNICOS',
        'aplicacoes'           => 'Algumas aplicações',
        'titulo-aplicacoes'    => 'APLICAÇÕES',
        'busca'                => "Informações sobre os têxteis técnicos",
    ],

    // INFORMAÇÃO
    'informacao' => [
        'titulo'           => 'INFORMAÇÃO',
        'publicacoes'      => 'Publicações',
        'cursos-e-eventos' => 'Cursos e Eventos',
        'normas-e-ensaios' => 'Normas e Ensaios',
        'imprensa'         => 'Imprensa',
        'outros'           => 'Outros',
        'visitar'          => 'VISITAR O LINK',
        'ler-mais'         => 'LER MAIS',
    ],

    // ASSOCIE-SE
    'associe-se' => [
        'titulo'   => 'ASSOCIE-SE',
        'missao'   => 'MISSÃO',
        'visao'    => 'VISÃO',
        'pilares'  => 'NOSSOS PILARES',
        'frase'    => 'Receba mais informações para realizar sua associação:',
    ],

    // BUSCA
    'busca' => [
        'titulo'      => 'RESULTADOS DA BUSCA',
        'nenhum'      => 'Não foi encontrado nenhum resultado para a sua busca.',
        'saiba-mais'  => 'Clique aqui e saiba mais...',
    ],

    // CONTATO
    'contato' => [
        'titulo'   => 'CONTATO',
        'email'    => 'e-mail',
        'nome'     => 'nome',
        'telefone' => 'telefone',
        'mensagem' => 'mensagem',
        'sucesso'  => 'Mensagem enviada com sucesso!',
        'pais'     => 'Brasil',
        'voltar'   => 'voltar',
    ],

    // Footer
    'footer' => [
        'direitos'      => 'Todos os direitos reservados.',
        'criacao-sites' => 'Criação de Sites: ',
        'politica'      => 'Política de Privacidade',
        'diretoria'     => 'Diretoria',
        'comites'       => 'Comitês Técnicos',
    ],

    'cookies-parte1' => 'Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa ',
    'cookies-parte2' => ' e saiba mais.',
    'cookies-btn'    => 'ACEITAR E FECHAR',

];
