<?php

return [

    'nome'           => 'nome',
    'endereco'       => 'endereco',
    'description'    => 'description',
    'descricao'      => 'descricao',
    'titulo'         => 'titulo',
    'itens'          => 'itens',
    'texto'          => 'texto',
    'periodo'        => 'periodo',
    'horario'        => 'horario',
    'local'          => 'local',
    'bienio_periodo' => 'bienio_periodo',
    'titulo_arquivo' => 'titulo_arquivo',
    'titulo_motivos' => 'titulo_motivos',
    'm1_titulo'      => 'm1_titulo',
    'm1_texto'       => 'm1_texto',
    'm2_titulo'      => 'm2_titulo',
    'm2_texto'       => 'm2_texto',
    'm3_titulo'      => 'm3_titulo',
    'm3_texto'       => 'm3_texto',
    'm4_titulo'      => 'm4_titulo',
    'm4_texto'       => 'm4_texto',
    'missao'         => 'missao',
    'visao'          => 'visao',
    'pilares'        => 'pilares',
];
