<?php

return [

    // Header NAV 
    'header' => [
        'home'             => 'HOME',
        'a-abint'          => 'ABINT',
        'associados'       => 'ASSOCIATED',
        'naotecidos'       => 'NONWOVENS',
        'texteis-tecnicos' => 'TECHNICAL TEXTILES',
        'informacao'       => 'INFORMATION',
        'publicacoes'      => 'Publications',
        'cursos-eventos'   => 'Courses and Events',
        'normas-ensaios'   => 'Standards and Tests',
        'imprensa'         => 'Press',
        'outros'           => 'Others',
        'associe-se'       => 'JOIN',
        'contato'          => 'CONTACT',
        'buscar'           => 'search...',
    ],

    // HOME
    'home' => [
        'titulo-objetivos' => 'GOALS',
        'objetivos'        => 'Goals of ABINT',
        'busca-objetivos'  => "Click here and learn more about ABINT's objectives...",
        'saiba-mais'       => 'LEARN MORE ABOUT ABINT ',
        'naotecidos'       => 'nonwovens',
        'tecidos-tecnicos' => 'technical textiles',
        'publicacoes'      => 'publications',
        'mais-informacoes' => 'more information',
    ],

    // A ABINT
    'abint' => [
        'titulo'         => 'ABINT',
        'titulo-busca'   => 'About ABINT',
        'diretoria'      => 'Board',
        'presidente'     => 'President:',
        'vice'           => 'Vice-President:',
        'conselheiros'   => 'Counselors:',
        'diretores'      => 'Directors:',
        'titulo-comites' => 'COMMITTEES',
        'comites'        => 'Technical committees',
        'busca-comites'  => "Click here and learn more about this technical committee...",
        'empresa'        => 'Company: ',
        'busca-abint'    => ". Click here and learn more about ABINT's board...",
    ],

    // ASSOCIADOS
    'associados' => [
        'titulo'           => 'ASSOCIATED',
        'maquinas'         => 'Machines and Equipment',
        'mat-primas'       => 'Raw materials and Inputs',
        'fab-naotecidos'   => 'Nonwovens Manufacturers',
        'fab-texteis-tec'  => 'Technical Fabric and/or Textile Manufacturers',
        'convertedores'    => 'Converters and Distributors',
        'busca'            => ". Click here and see all members...",
        'busca-grupo'      => "Click here and see all members of this group...",
        'link-texto'       => "KNOW ALL ABINT ASSOCIATED COMPANIES",
    ],

    // NAOTECIDOS
    'naotecidos' => [
        'titulo'                => 'NONWOVENS',
        'o-que-sao'             => 'What are nonwovens',
        'titulo-o-que-sao'      => 'WHAT ARE NONWOVENS',
        'historia'              => 'History',
        'titulo-historia'       => 'THE HISTORY OF THE NONWOVENS',
        'mat-primas'            => 'Raw material',
        'titulo-mat-primas'     => 'RAW MATERIALS USED',
        'fabricacao'            => 'How they are manufactured',
        'titulo-fabricacao'     => 'HOW NONWOVENS ARE MANUFACTURED',
        'transformacao'         => 'Transformation, finishing and/or conversion',
        'titulo-transformacao'  => 'TRANSFORMATION, FINISHING AND/OR CONVERSION OF UNTECTED',
        'classificacao'         => 'Classification',
        'titulo-classificacao'  => 'CLASSIFICATION OF NONWOVEN',
        'identificacao'         => 'Identification',
        'titulo-identificacao'  => 'IDENTIFICATION OF THE NONWOVEN',
        'algumas-aplicacoes'    => 'Some applications',
        'titulo-aplicacoes'     => 'SOME APPLICATIONS',
        'aplicacoes-a-z'        => 'Applications from A to Z',
        'titulo-aplicacoes-a-z' => 'APPLICATIONS FROM A TO Z',
        'frase-aplicacoes-a-z'  => 'Click on the letters and get to know some products where Nonwovens are used.',
        'busca'                 => "Information about nonwovens",
    ],

    // TEXTEIS TECNICOS
    'texteis-tec' => [
        'titulo'               => 'TECHNICAL TEXTILES',
        'o-que-sao'            => 'What are technical textiles',
        'titulo-o-que-sao'     => 'WHAT ARE TECHNICAL TEXTILES',
        'mat-primas'           => 'Raw material',
        'titulo-mat-primas'    => 'RAW MATERIALS USED',
        'fabricacao'           => 'How they are manufactured',
        'titulo-fabricacao'    => 'HOW TECHNICAL TEXTILES ARE MANUFACTURED',
        'processos'            => 'Transformation and finishing processes',
        'titulo-processos'     => 'TRANSFORMATION AND FINISHING PROCESSES',
        'classificacao'        => 'Classification',
        'titulo-classificacao' => 'CLASSIFICATION OF TECHNICAL TEXTILES',
        'desempenho'           => 'Performance',
        'titulo-desempenho'    => 'DETERMINANTS OF TECHNICAL TEXTILE PERFORMANCE',
        'identificacao'        => 'Identification',
        'titulo-identificacao' => 'IDENTIFICATION OF TECHNICAL TEXTILES',
        'aplicacoes'           => 'Some applications',
        'titulo-aplicacoes'    => 'APPLICATIONS',
        'busca'                => "Information on technical textiles",
    ],

    // INFORMAÇÃO
    'informacao' => [
        'titulo'           => 'Information',
        'publicacoes'      => 'Publications',
        'cursos-e-eventos' => 'Courses and Events',
        'normas-e-ensaios' => 'Standards and Tests',
        'imprensa'         => 'Press',
        'outros'           => 'Others',
        'visitar'          => 'VISIT THE LINK',
        'ler-mais'         => 'READ MORE',
    ],

    // ASSOCIE-SE
    'associe-se' => [
        'titulo'   => 'JOIN',
        'missao'   => 'MISSION',
        'visao'    => 'VISION',
        'pilares'  => 'OUR PILLARS',
        'frase'    => 'Receive more information to make your association:',
    ],

    // BUSCA
    'busca' => [
        'titulo'      => 'SEARCH RESULTS',
        'nenhum'      => 'No results were found for your search.',
        'saiba-mais'  => 'Click here to learn more...',
    ],

    // CONTATO
    'contato' => [
        'titulo'   => 'CONTACT',
        'email'    => 'e-mail',
        'nome'     => 'name',
        'telefone' => 'telephone',
        'mensagem' => 'message',
        'sucesso'  => 'Message sent successfully!',
        'pais'     => 'Brazil',
        'voltar'   => 'come back',
    ],

    // Footer
    'footer' => [
        'direitos'      => 'All rights reserved.',
        'criacao-sites' => 'Sites creation: ',
        'politica'      => 'Privacy Policy',
        'diretoria'     => 'Board',
        'comites'       => 'Technical Committees',
    ],

    'cookies-parte1' => 'We use cookies to personalize content, track ads and provide a safer browsing experience for you. By continuing to browse our website, you agree to our use of this information. read our ',
    'cookies-parte2' => ' and learn more.',
    'cookies-btn'    => 'ACCEPT AND CLOSE',

];
