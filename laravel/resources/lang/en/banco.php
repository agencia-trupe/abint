<?php

return [

    'nome'           => 'nome_en',
    'endereco'       => 'endereco_en',
    'description'    => 'description_en',
    'descricao'      => 'descricao_en',
    'titulo'         => 'titulo_en',
    'itens'          => 'itens_en',
    'texto'          => 'texto_en',
    'periodo'        => 'periodo_en',
    'horario'        => 'horario_en',
    'local'          => 'local_en',
    'bienio_periodo' => 'bienio_periodo_en',
    'titulo_arquivo' => 'titulo_arquivo_en',
    'titulo_motivos' => 'titulo_motivos_en',
    'm1_titulo'      => 'm1_titulo_en',
    'm1_texto'       => 'm1_texto_en',
    'm2_titulo'      => 'm2_titulo_en',
    'm2_texto'       => 'm2_texto_en',
    'm3_titulo'      => 'm3_titulo_en',
    'm3_texto'       => 'm3_texto_en',
    'm4_titulo'      => 'm4_titulo_en',
    'm4_texto'       => 'm4_texto_en',
    'missao'         => 'missao_en',
    'visao'          => 'visao_en',
    'pilares'        => 'pilares_en',

];
