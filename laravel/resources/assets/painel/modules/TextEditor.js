const config = {
  padrao: {
    toolbar: [["Bold", "Italic"], ["RemoveFormat"]],
  },

  clean: {
    toolbar: [],
    removePlugins: "toolbar,elementspath",
  },

  cleanBr: {
    toolbar: [],
    removePlugins: "toolbar,elementspath",
    enterMode: CKEDITOR.ENTER_BR,
  },

  textImage: {
    toolbar: [
      ["Bold", "Italic"],
      ["Format"],
      ["TextColor", "BGColor"],
      ["BulletedList", "NumberedList"],
      ["Link", "Unlink"],
      ["InjectImage"],
      ["RemoveFormat"],
    ],
  },

  textItens: {
    toolbar: [
      ["Bold", "Italic"],
      ["BulletedList"],
      ["Link", "Unlink"],
      ["RemoveFormat"],
    ],
  },
};

export default function TextEditor() {
  CKEDITOR.config.language = "pt-br";
  CKEDITOR.config.uiColor = "#dce4ec";
  CKEDITOR.config.contentsCss = [
    $("base").attr("href") + "/assets/ckeditor.css",
    CKEDITOR.config.contentsCss,
  ];
  CKEDITOR.config.removePlugins = "elementspath";
  CKEDITOR.config.resize_enabled = true;
  CKEDITOR.plugins.addExternal(
    "injectimage",
    $("base").attr("href") + "/assets/injectimage/plugin.js"
  );
  CKEDITOR.config.allowedContent = true;
  CKEDITOR.config.extraPlugins = "injectimage,colorbutton,removeformat";

  $(".ckeditor").each(function (i, obj) {
    CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
  });
}
