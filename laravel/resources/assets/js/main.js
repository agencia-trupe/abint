import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";

var moment = require("moment");

AjaxSetup();
MobileToggle();

$(document).ready(function () {
  var urlPrevia = "";
  // var urlPrevia = "/previa-abint";

  // SUBMENUS HEADER - 1º tablet e mobile - 2º demais
  if ($(window).width() < 1300) {
    $(
      "#menuAAbint, #menuAssociados, .nav-link#publicacoes, .nav-link#oQueSaoNT, .nav-link#oQueSaoTT"
    ).click(function (e) {
      e.preventDefault();
      $(".submenu-header").css("display", "none");
      $("header .nav-link.sub-active").removeClass("sub-active");
      var submenu = $(this).attr("submenu");
      $(this)
        .parent()
        .children("." + submenu)
        .css("display", "block");
      $(this).addClass("sub-active");

      if ($(this).attr("id") == "oQueSaoNT") {
        localStorage.clear();
        localStorage.setItem("naotecidosId", $(this).attr("id"));
      }
      if ($(this).attr("id") == "oQueSaoTT") {
        localStorage.clear();
        localStorage.setItem("naotecidosId", $(this).attr("id"));
      }
      if ($(this).attr("id") == "publicacoes") {
        localStorage.clear();
        localStorage.setItem("conteudotecId", $(this).attr("id"));
      }
    });
  } else {
    $(
      "#menuAAbint, #menuAssociados, .nav-link#publicacoes, .nav-link#oQueSaoNT, .nav-link#oQueSaoTT"
    ).mouseenter(function () {
      $(".submenu-header").css("display", "none");
      $("header .nav-link.sub-active").removeClass("sub-active");
      var submenu = $(this).attr("submenu");
      $(this)
        .parent()
        .children("." + submenu)
        .css("display", "block");
      $(this).addClass("sub-active");
    });
    $(
      ".submenu-a-abint, .submenu-associados, .submenu-informacao, .submenu-naotecidos, .submenu-texteis-tecnicos"
    ).mouseleave(function () {
      $(this).css("display", "none");
      $("header .nav-link.sub-active").removeClass("sub-active");
    });
  }

  // HOME - BANNERS
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    speed: 600,
    manualSpeed: 100,
    next: ".cycle-pager",
  });

  $(".home-destaques .centralizado").cycle({
    slides: ".destaque-row",
    fx: "fade",
    speed: 600,
    manualSpeed: 100,
    next: "#next",
    prev: "#prev"
  })

  // IDIOMAS - HEADER
  $(".idiomas-titulo").click(function (event) {
    event.stopPropagation();

    $(".idiomas-links").css("visibility", "visible");

    $(document).one("click", function (e) {
      if (!$(e.target).is(".MainNavContainer")) {
        $(".idiomas-links").css("visibility", "hidden");
      }
    });
  });

  // LUPA HEADER / BUSCA
  $(".lupa-header").click(function (e) {
    e.preventDefault();

    $(".campo-busca").show();
    e.stopPropagation();
  });
  $(window).scroll(function () {
    $(".campo-busca").hide();
  });
  $(".campo-busca").click(function (e) {
    e.stopPropagation();
  });
  $(document).click(function () {
    $(".campo-busca").hide();
  });

  // CLICKS PARA NÃOTECIDOS
  $(
    "header .submenu-naotecidos .submenu-link, header .nav-link#oQueSaoNT, footer .submenu-footer-naotecidos, footer .nav-footer#oQueSaoNT, home .item-conteudo#oQueSaoNT, .resultados-busca .link-result-naotecidos"
  ).click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("naotecidosId", $(this).attr("id"));
    if ($(window).width() >= 1300) {
      window.location.href = $(this).attr("href");
    }
  });
  var naotecidosId = localStorage.getItem("naotecidosId");
  console.log(naotecidosId);
  if (naotecidosId) {
    $(".naotecidos .submenu .link.active").removeClass("active");
    $(".naotecidos .submenu .link#" + naotecidosId).addClass("active");
  }

  // TROCA SUBMENU NÃOTECIDOS
  $(".naotecidos .submenu .link").click(function (e) {
    e.preventDefault();

    $(".naotecidos .submenu .link.active").removeClass("active");
    $(this).addClass("active");

    getDados(this.href);
  });

  var getDados = function (url) {
    $("#conteudoNaoTecidos").fadeOut("slow", function () {
      $.ajax({
        type: "GET",
        url: url,
        success: function (data, textStatus, jqXHR) {
          console.log(data);
          $("#conteudoNaoTecidos").html("").show();

          if (data.submenu == "algumas-aplicacoes") {
            var titulo = "<h2 class='titulo'>" + data.titulo + "</h2>";
            $("#conteudoNaoTecidos").append(titulo);

            data.resultado.forEach((element) => {
              if ($(".idiomas-titulo").attr("idioma") == "en") {
                var html =
                  "<div class='alguma-aplicacao'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/naotecidos-aplicacoes/" +
                  element.imagem +
                  "' class='img-aplicacao' alt='" +
                  element.titulo_en +
                  "'><div class='dados-aplicacao'><h4 class='titulo'>" +
                  element.titulo_en +
                  "</h4><div class='texto-aplicacao'>" +
                  element.texto_en +
                  "</div></div></div>";
                $("#conteudoNaoTecidos").append(html);
              } else {
                var html =
                  "<div class='alguma-aplicacao'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/naotecidos-aplicacoes/" +
                  element.imagem +
                  "' class='img-aplicacao' alt='" +
                  element.titulo +
                  "'><div class='dados-aplicacao'><h4 class='titulo'>" +
                  element.titulo +
                  "</h4><div class='texto-aplicacao'>" +
                  element.texto +
                  "</div></div></div>";
                $("#conteudoNaoTecidos").append(html);
              }
            });
          } else if (data.submenu == "aplicacoes-a-z") {
            var titulo = "<h2 class='titulo'>" + data.titulo + "</h2>";
            $("#conteudoNaoTecidos").append(titulo);

            var html =
              "<div class='aplicacoes-a-z'><p class='frase'>" +
              fraseAplicacoesAZ +
              "</p><div class='letras' id='listaLetras'></div><div class='resultados' id='resultadosLetra'></div></div>";
            $("#conteudoNaoTecidos").append(html);

            data.letras.forEach((letra) => {
              var linkHtml =
                "<a href='#' class='letra-aplicacao' id='" +
                letra +
                "'>" +
                letra +
                "</a>";
              $("#listaLetras").append(linkHtml);
            });
          } else {
            if ($(".idiomas-titulo").attr("idioma") == "en") {
              var html =
                "<h2 class='titulo'>" +
                data.titulo +
                "</h2><div class='texto'>" +
                data.resultado.texto_en +
                "</div>";
              $("#conteudoNaoTecidos").append(html);
            } else {
              var html =
                "<h2 class='titulo'>" +
                data.titulo +
                "</h2><div class='texto'>" +
                data.resultado.texto +
                "</div>";
              $("#conteudoNaoTecidos").append(html);
            }
          }

          window.history.pushState(
            "",
            "",
            urlPrevia + "/naotecidos/" + data.submenu
          );
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  };

  // CLIQUE LETRAS - NAOTECIDOS/APLICACOES-A-Z
  $(document).on("click", ".letra-aplicacao", function (e) {
    e.preventDefault();

    $(".letra-aplicacao.active").removeClass("active");
    $(this).addClass("active");

    getAplicacaoLetra($(this).html());
  });

  var getAplicacaoLetra = function (letra) {
    var url =
      window.location.origin +
      urlPrevia +
      "/naotecidos/aplicacoes-a-z/" +
      letra;

    $("#resultadosLetra").fadeOut("slow", function () {
      $.ajax({
        type: "GET",
        url: url,
        success: function (data, textStatus, jqXHR) {
          $("#resultadosLetra").html("").show();

          if ($(".idiomas-titulo").attr("idioma") == "en") {
            data.resultados.forEach((element) => {
              fillResultadosLetras(
                element.titulo_en,
                element.itens_en,
                element.id
              );
            });
          } else {
            data.resultados.forEach((element) => {
              fillResultadosLetras(element.titulo, element.itens, element.id);
            });
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  };

  // CLICKS PARA TÊXTEIS TÉCNICOS
  $(
    "header .submenu-texteis-tecnicos .submenu-link, header .nav-link#oQueSaoTT, footer .submenu-footer-texteis-tecnicos, footer .nav-footer#oQueSaoTT, home .item-conteudo#oQueSaoTT, .resultados-busca .link-result-texteis-tecnicos"
  ).click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("texteistecId", $(this).attr("id"));
    if ($(window).width() >= 1300) {
      window.location.href = $(this).attr("href");
    }
  });
  var texteistecId = localStorage.getItem("texteistecId");
  if (texteistecId) {
    $(".texteis-tecnicos .submenu .link.active").removeClass("active");
    $(".texteis-tecnicos .submenu .link#" + texteistecId).addClass("active");
  }

  // TROCA SUBMENU TÊXTEIS TECNICOS
  $(".texteis-tecnicos .submenu .link").click(function (e) {
    e.preventDefault();

    $(".texteis-tecnicos .submenu .link.active").removeClass("active");
    $(this).addClass("active");

    getDadosTexteis(this.href);
  });

  var getDadosTexteis = function (url) {
    $("#conteudoTexteisTecnicos").fadeOut("slow", function () {
      $.ajax({
        type: "GET",
        url: url,
        success: function (data, textStatus, jqXHR) {
          console.log(data);
          $("#conteudoTexteisTecnicos").html("").show();

          if ($(".idiomas-titulo").attr("idioma") == "en") {
            var html =
              "<h2 class='titulo'>" +
              data.titulo +
              "</h2><div class='texto'>" +
              data.resultado.texto_en +
              "</div>";
            $("#conteudoTexteisTecnicos").append(html);
          } else {
            var html =
              "<h2 class='titulo'>" +
              data.titulo +
              "</h2><div class='texto'>" +
              data.resultado.texto +
              "</div>";
            $("#conteudoTexteisTecnicos").append(html);
          }

          window.history.pushState(
            "",
            "",
            urlPrevia + "/texteis-tecnicos/" + data.submenu
          );
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  };

  var fillResultadosLetras = function (titulo, itens, id) {
    console.log(itens);
    var html =
      "<div class='item-aplicacao'><h4 class='titulo'>" +
      titulo +
      "</h4><ul class='lista-itens' id='itensResultadosLetra" +
      id +
      "'></ul></div>";
    $("#resultadosLetra").append(html);

    var itensLista = itens.split("|");
    console.log(itensLista);

    if (itensLista.length > 0 && itensLista[0] != "") {
      itensLista.forEach((item) => {
        var liHtml = "<li class='item'>" + item + "</li>";
        $("#itensResultadosLetra" + id).append(liHtml);
      });
    } else {
      $("#itensResultadosLetra" + id).hide();
    }
  };

  // CLICKS PARA INFORMAÇÃO
  $(
    "header .submenu-informacao .submenu-link, header .nav-link#publicacoes, footer .submenu-footer-informacao, footer .nav-footer#publicacoes, .home .item-conteudo#publicacoes, .resultados-busca .link-result-informacao, .informacao .submenu .link-show, .informacao .conteudo .novidade, .informacao .conteudo .outro, .btn-voltar-imprensa, .btn-voltar-outros"
  ).click(function (e) {
    e.preventDefault();
    localStorage.clear();
    localStorage.setItem("conteudotecId", $(this).attr("id"));
    if ($(window).width() >= 1300) {
      window.location.href = $(this).attr("href");
    }
  });
  var conteudotecId = localStorage.getItem("conteudotecId");
  if (conteudotecId) {
    $(".informacao .submenu .link.active").removeClass("active");
    $(".informacao .submenu .link#" + conteudotecId).addClass("active");
  }

  // TROCA SUBMENU INFORMAÇÃO
  $(".informacao .submenu .link").click(function (e) {
    e.preventDefault();

    $(".informacao .submenu .link.active").removeClass("active");
    $(this).addClass("active");

    getDadosInformacao(this.href);
  });

  var getDadosInformacao = function (url) {
    $("#conteudoInformacao").fadeOut("slow", function () {
      $.ajax({
        type: "GET",
        url: url,
        success: function (data, textStatus, jqXHR) {
          console.log(data);
          $("#conteudoInformacao").html("").show();

          var htmlTitulo = "<h2 class='titulo'>" + data.titulo + "</h2>";
          $("#conteudoInformacao").append(htmlTitulo);

          if (data.submenu == "publicacoes") {
            data.resultado.forEach((element) => {
              if ($(".idiomas-titulo").attr("idioma") == "en") {
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/publicacoes/download/" +
                  element.slug +
                  "' target='_blank' class='publicacao' id='" +
                  element.id +
                  "'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/publicacoes/" +
                  element.imagem +
                  "' class='img-publicacao' alt='" +
                  element.titulo_en +
                  "'><div class='dados'><h4 class='titulo'>" +
                  element.titulo_en +
                  "</h4><div class='descricao'>" +
                  element.descricao_en +
                  "</div></div><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/ico-pdf.svg" +
                  "' class='img-download' alt=''></div>";
              } else {
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/publicacoes/download/" +
                  element.slug +
                  "' target='_blank' class='publicacao' id='" +
                  element.id +
                  "'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/publicacoes/" +
                  element.imagem +
                  "' class='img-publicacao' alt='" +
                  element.titulo +
                  "'><div class='dados'><h4 class='titulo'>" +
                  element.titulo +
                  "</h4><div class='descricao'>" +
                  element.descricao +
                  "</div></div><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/ico-pdf.svg" +
                  "' class='img-download' alt=''></div>";
              }
              $("#conteudoInformacao").append(html);
            });
          } else if (data.submenu == "cursos-e-eventos") {
            data.resultado.forEach((element) => {
              if ($(".idiomas-titulo").attr("idioma") == "en") {
                var html =
                  "<div class='curso-evento' id='" +
                  element.id +
                  "'><a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/eventos/originais/" +
                  element.imagem +
                  "' class='fancybox'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/eventos/" +
                  element.imagem +
                  "' class='img-curso-evento' alt='" +
                  element.nome_en +
                  "'></a><div class='dados'><h4 class='titulo'>" +
                  element.nome_en +
                  "</h4><p class='periodo'>" +
                  element.periodo_en +
                  "</p><div class='descricao'>" +
                  element.descricao_en +
                  "</div><a href='" +
                  element.link +
                  "' target='_blank' class='link-texto'>" +
                  element.link +
                  "</a></div><a href='" +
                  element.link +
                  "' target='_blank' class='icones'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/ico-visitarsite-laranja.svg" +
                  "' class='img-visitar' alt=''><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/icone-linkseta.svg" +
                  "' class='img-seta' alt=''></a></div>";
              } else {
                var html =
                  "<div class='curso-evento' id='" +
                  element.id +
                  "'><a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/eventos/originais/" +
                  element.imagem +
                  "' class='fancybox'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/eventos/" +
                  element.imagem +
                  "' class='img-curso-evento' alt='" +
                  element.nome +
                  "'></a><div class='dados'><h4 class='titulo'>" +
                  element.nome +
                  "</h4><p class='periodo'>" +
                  element.periodo +
                  "</p><div class='descricao'>" +
                  element.descricao +
                  "</div><a href='" +
                  element.link +
                  "' target='_blank' class='link-texto'>" +
                  element.link +
                  "</a></div><a href='" +
                  element.link +
                  "' target='_blank' class='icones'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/ico-visitarsite-laranja.svg" +
                  "' class='img-visitar' alt=''><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/icone-linkseta.svg" +
                  "' class='img-seta' alt=''></a></div>";
              }
              $("#conteudoInformacao").append(html);
            });
          } else if (data.submenu == "normas-e-ensaios") {
            data.resultado.forEach((element) => {
              if ($(".idiomas-titulo").attr("idioma") == "en") {
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/normas-e-ensaios/download/" +
                  element.slug +
                  "' target='_blank' class='norma-ensaio' id='" +
                  element.id +
                  "'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/normas-e-ensaios/" +
                  element.imagem +
                  "' class='img-norma-ensaio' alt='" +
                  element.titulo_en +
                  "'><div class='dados'><h4 class='titulo'>" +
                  element.titulo_en +
                  "</h4><div class='descricao'>" +
                  element.descricao_en +
                  "</div></div><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/ico-pdf.svg" +
                  "' class='img-download' alt=''></a>";
              } else {
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/normas-e-ensaios/download/" +
                  element.slug +
                  "' target='_blank' class='norma-ensaio' id='" +
                  element.id +
                  "'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/normas-e-ensaios/" +
                  element.imagem +
                  "' class='img-norma-ensaio' alt='" +
                  element.titulo +
                  "'><div class='dados'><h4 class='titulo'>" +
                  element.titulo +
                  "</h4><div class='descricao'>" +
                  element.descricao +
                  "</div></div><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/ico-pdf.svg" +
                  "' class='img-download' alt=''></a>";
              }
              $("#conteudoInformacao").append(html);
            });
          } else if (data.submenu == "imprensa") {
            data.resultado.forEach((element) => {
              if ($(".idiomas-titulo").attr("idioma") == "en") {
                var objData = new Date(element.data);
                var strData = moment(objData)
                  .locale("en")
                  .add(1, "days")
                  .format("DD MMMM YYYY");
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/imprensa/" +
                  element.slug +
                  "' class='novidade' id='imprensa'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/novidades/lista/" +
                  element.imagem +
                  "' class='img-novidade' alt='" +
                  element.titulo_en +
                  "'><div class='dados'><p class='data'>" +
                  strData +
                  "</p><h4 class='titulo'>" +
                  element.titulo_en +
                  "</h4></div><div class='ler-mais'><div class='triangulo'></div><div class='retangulo'>LER MAIS » <img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/icone-linkseta-branco.svg/" +
                  "'  alt='' class='img-site'></div></div></div>";
              } else {
                var objData = new Date(element.data);
                var strData = moment(objData)
                  .locale("pt-BR")
                  .add(1, "days")
                  .format("DD MMMM YYYY");
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/imprensa/" +
                  element.slug +
                  "' class='novidade' id='imprensa'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/novidades/lista/" +
                  element.imagem +
                  "' class='img-novidade' alt='" +
                  element.titulo +
                  "'><div class='dados'><p class='data'>" +
                  strData +
                  "</p><h4 class='titulo'>" +
                  element.titulo +
                  "</h4></div><div class='ler-mais'><div class='triangulo'></div><div class='retangulo'>LER MAIS » <img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/icone-linkseta-branco.svg/" +
                  "'  alt='' class='img-site'></div></div></div>";
              }
              $("#conteudoInformacao").append(html);
            });
          } else if (data.submenu == "outros") {
            data.resultado.forEach((element) => {
              if ($(".idiomas-titulo").attr("idioma") == "en") {
                var objData = new Date(element.data);
                var strData = moment(objData)
                  .locale("en")
                  .add(1, "days")
                  .format("DD MMMM YYYY");
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/outros/" +
                  element.slug +
                  "' class='outro' id='outros'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/outros/lista/" +
                  element.imagem +
                  "' class='img-outro' alt='" +
                  element.titulo_en +
                  "'><div class='dados'><p class='data'>" +
                  strData +
                  "</p><h4 class='titulo'>" +
                  element.titulo_en +
                  "</h4></div><div class='ler-mais'><div class='triangulo'></div><div class='retangulo'>LER MAIS » <img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/icone-linkseta-branco.svg/" +
                  "'  alt='' class='img-site'></div></div></div>";
              } else {
                var objData = new Date(element.data);
                var strData = moment(objData)
                  .locale("pt-BR")
                  .add(1, "days")
                  .format("DD MMMM YYYY");
                var html =
                  "<a href='" +
                  window.location.origin +
                  urlPrevia +
                  "/informacao/outros/" +
                  element.slug +
                  "' class='outro' id='outros'><img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/outros/lista/" +
                  element.imagem +
                  "' class='img-outro' alt='" +
                  element.titulo +
                  "'><div class='dados'><p class='data'>" +
                  strData +
                  "</p><h4 class='titulo'>" +
                  element.titulo +
                  "</h4></div><div class='ler-mais'><div class='triangulo'></div><div class='retangulo'>LER MAIS » <img src='" +
                  window.location.origin +
                  urlPrevia +
                  "/assets/img/layout/icone-linkseta-branco.svg/" +
                  "'  alt='' class='img-site'></div></div></div>";
              }
              $("#conteudoInformacao").append(html);
            });
          }

          window.history.pushState(
            "",
            "",
            urlPrevia + "/informacao/" + data.submenu
          );
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR, textStatus, errorThrown);
        },
      });
    });
  };

  // INFORMAÇÃO - FANCYBOX CURSOS-E-EVENTOS + OUTROS
  $(".fancybox").fancybox({
    padding: 0,
    prevEffect: "fade",
    nextEffect: "fade",
    closeBtn: false,
    openEffect: "elastic",
    openSpeed: "150",
    closeEffect: "elastic",
    closeSpeed: "150",
    helpers: {
      title: {
        type: "outside",
        position: "top",
      },
      overlay: {
        css: {
          background: "rgba(132, 134, 136, .88)",
        },
      },
    },
    fitToView: false,
    autoSize: false,
    beforeShow: function () {
      this.maxWidth = "90%";
      this.maxHeight = "90%";
    },
  });

  // localStorage
  var naotecidosId = localStorage.getItem("naotecidosId");
  var texteistecId = localStorage.getItem("texteistecId");

  if (naotecidosId) {
    $(".naotecidos .submenu .link#" + naotecidosId).addClass("active");
  }
  if (texteistecId) {
    $(".texteis-tecnicos .submenu .link#" + texteistecId).addClass("active");
  }

  // AVISO DE COOKIES
  $(".aviso-cookies").show();

  $(".aceitar-cookies").click(function () {
    var url = window.location.origin + "/aceite-de-cookies";

    $.ajax({
      type: "POST",
      url: url,
      success: function (data, textStatus, jqXHR) {
        $(".aviso-cookies").hide();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR, textStatus, errorThrown);
      },
    });
  });
});
