@extends('frontend.common.template')

@section('content')

<div class="informacao">
    <!-- troca de submenus via AJAX -->
    <h1 class="titulo">{{ trans('frontend.informacao.titulo') }}</h1>
    <div class="centralizado">
        <div class="submenu">
            <a href="{{ route('informacao.index', ['submenu' => 'publicacoes']) }}" class="link-show" id="publicacoes">» {{ trans('frontend.informacao.publicacoes') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'cursos-e-eventos']) }}" class="link-show" id="cursosEEventos">» {{ trans('frontend.informacao.cursos-e-eventos') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'normas-e-ensaios']) }}" class="link-show" id="normasEEnsaios">» {{ trans('frontend.informacao.normas-e-ensaios') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'imprensa']) }}" class="link-show active" id="imprensa">» {{ trans('frontend.informacao.imprensa') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'outros']) }}" class="link-show" id="outros">» {{ trans('frontend.informacao.outros') }}</a>
        </div>
        <div class="conteudo" id="conteudoNovidadeShow">
            <h2 class="titulo">{{ trans('frontend.informacao.imprensa') }}</h2>

            <img src="{{ asset('assets/img/novidades/'.$novidade->imagem) }}" class="capa-novidade" alt="{{ $novidade->{trans('banco.titulo')} }}">
            <p class="data-novidade">{{ strftime("%d %b %Y", strtotime($novidade->data)) }}</p>
            <p class="titulo-novidade">{{ $novidade->{trans('banco.titulo')} }}</p>
            <div class="texto-novidade">{!! $novidade->{trans('banco.texto')} !!}</div>
            @if(isset($novidade->arquivo))
            <a href="{{ route('novidades.download', $novidade->slug) }}" target="_blank" class="link-download-novidade" id="{{ $novidade->id }}">
                <img src="{{ asset('assets/img/layout/icone-PDF.svg') }}" class="img-pdf" alt="Download">
                {{ $novidade->{trans('banco.titulo_arquivo')} }}
            </a>
            @endif
            <a href="{{ route('informacao.index', ['submenu' => 'imprensa']) }}" class="btn-voltar-imprensa" id="imprensa">« VOLTAR</a>
        </div>
    </div>

    @include('frontend.associados-link')

</div>

@endsection