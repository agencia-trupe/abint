@extends('frontend.common.template')

@section('content')

<div class="politica-de-privacidade">
    <div class="centralizado">
        <h2 class="titulo">{{ trans('frontend.footer.politica') }}</h2>
        <div class="texto">{!! $politica->{trans('banco.texto')} !!}</div>
    </div>

    @include('frontend.associados-link')
</div>

@endsection