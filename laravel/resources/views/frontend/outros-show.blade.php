@extends('frontend.common.template')

@section('content')

<div class="informacao">
    <!-- troca de submenus via AJAX -->
    <h1 class="titulo">{{ trans('frontend.informacao.titulo') }}</h1>
    <div class="centralizado">
        <div class="submenu">
            <a href="{{ route('informacao.index', ['submenu' => 'publicacoes']) }}" class="link-show" id="publicacoes">» {{ trans('frontend.informacao.publicacoes') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'cursos-e-eventos']) }}" class="link-show" id="cursosEEventos">» {{ trans('frontend.informacao.cursos-e-eventos') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'normas-e-ensaios']) }}" class="link-show" id="normasEEnsaios">» {{ trans('frontend.informacao.normas-e-ensaios') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'imprensa']) }}" class="link-show" id="imprensa">» {{ trans('frontend.informacao.imprensa') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'outros']) }}" class="link-show active" id="outros">» {{ trans('frontend.informacao.outros') }}</a>
        </div>
        <div class="conteudo" id="conteudoOutroShow">
            <h2 class="titulo">{{ trans('frontend.informacao.outros') }}</h2>

            <img src="{{ asset('assets/img/outros/'.$outro->imagem) }}" class="capa-outro" alt="{{ $outro->{trans('banco.titulo')} }}">
            <p class="data-outro">{{ strftime("%d %b %Y", strtotime($outro->data)) }}</p>
            <p class="titulo-outro">{{ $outro->{trans('banco.titulo')} }}</p>
            <div class="texto-outro">{!! $outro->{trans('banco.texto')} !!}</div>
            @if(isset($outro->arquivo))
            <a href="{{ route('outros.download', $outro->slug) }}" target="_blank" class="link-download-outro" id="{{ $outro->id }}">
                <img src="{{ asset('assets/img/layout/icone-PDF.svg') }}" class="img-pdf" alt="Download">
                {{ $outro->{trans('banco.titulo_arquivo')} }}
            </a>
            @endif
            <a href="{{ route('informacao.index', ['submenu' => 'outros']) }}" class="btn-voltar-outros" id="outros">« VOLTAR</a>
        </div>
    </div>

    @include('frontend.associados-link')

</div>

@endsection