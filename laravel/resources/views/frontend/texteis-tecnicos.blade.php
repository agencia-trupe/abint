@extends('frontend.common.template')

@section('content')

<div class="texteis-tecnicos">
    <h1 class="titulo">{{ trans('frontend.texteis-tec.titulo') }}</h1>
    <div class="centralizado">
        <div class="submenu">
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'o-que-sao']) }}" class="link active" id="oQueSaoTT">» {{ trans('frontend.texteis-tec.o-que-sao') }}</a>
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'materias-primas']) }}" class="link" id="matPrimaTT">» {{ trans('frontend.texteis-tec.mat-primas') }}</a>
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'como-sao-fabricados']) }}" class="link" id="comoSaoFabTT">» {{ trans('frontend.texteis-tec.fabricacao') }}</a>
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'processo-transf-acabamento']) }}" class="link" id="processoTransfAcab">» {{ trans('frontend.texteis-tec.processos') }}</a>
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'classificacao']) }}" class="link" id="classificacaoTT">» {{ trans('frontend.texteis-tec.classificacao') }}</a>
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'desempenho']) }}" class="link" id="desempenhoTT">» {{ trans('frontend.texteis-tec.desempenho') }}</a>
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'identificacao']) }}" class="link" id="identificacaoTT">» {{ trans('frontend.texteis-tec.identificacao') }}</a>
            <a href="{{ route('texteis-tecnicos.getDadosTexteis', ['submenu' => 'algumas-aplicacoes']) }}" class="link" id="algumasAplicacoesTT">» {{ trans('frontend.texteis-tec.aplicacoes') }}</a>
        </div>
        <div class="conteudo" id="conteudoTexteisTecnicos">
            <h2 class="titulo">{{ $titulo }}</h2>
            <div class="texto">{!! $resultado->{trans('banco.texto')} !!}</div>
            <!-- troca de submenus via AJAX -->
        </div>
    </div>

    @include('frontend.associados-link')

</div>

@endsection