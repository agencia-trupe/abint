@extends('frontend.common.template')

@section('content')

<div class="home">

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})">
            <div class="textos-banners">
                <p class="frase-banner">{{ $banner->{trans('banco.titulo')} }}</p>
                <div class="itens-banner">
                    @php $itensBanner = explode("|", $banner->{trans('banco.itens')}); @endphp
                    @foreach($itensBanner as $item)
                    <p class="item-banner">» {{ $item }}</p>
                    @endforeach
                </div>
            </div>
        </div>
        @endforeach
        <img src="{{ asset('assets/img/layout/seta-banners-home.svg') }}" alt="" class="cycle-pager">
    </div>

    @if(count($destaques) > 0)
    <div class="home-destaques">
        <div class="centralizado" data-cycle-prev="#prev" data-cycle-next="#next">
            <img src="{{ asset('assets/img/layout/seta-contato.svg') }}" alt="" class="img-prev" id="prev">
            @foreach($destaques->chunk(3) as $row)
            <div class="destaque-row">
                @foreach($row as $destaque)
                <a href="{{ $destaque->link }}" class="link-destaque" target="_blank">
                    <img src="{{ asset('assets/img/destaques/'.$destaque->imagem) }}" class="img-destaque" alt="{{ $destaque->link }}">
                </a>
                @endforeach
            </div>
            @endforeach
            <img src="{{ asset('assets/img/layout/seta-contato.svg') }}" alt="" class="img-next" id="next">
        </div>
    </div>
    @endif

    @if(count($novidades) > 0)
    <div class="home-novidades">
        @foreach($novidades as $novidade)
        <div class="novidade">
            <img src="{{ asset('assets/img/novidades/home/'.$novidade->imagem) }}" class="img-novidade" alt="{{$novidade->titulo}}">
            <p class="titulo-novidade">{{ $novidade->{trans('banco.titulo')} }}</p>
            <p class="data-novidade">{{ strftime("%d %b %Y", strtotime($novidade->data)) }}</p>
            <div class="btn-info">
                <a href="{{ route('novidades.show', $novidade->slug) }}" class="mais-informacoes">{{ trans('frontend.home.mais-informacoes') }}</a>
                <div class="triangulo"></div>
            </div>
        </div>
        @endforeach
    </div>
    @endif

    <div class="home-objetivos">
        <h1 class="titulo">{{ trans('frontend.home.objetivos') }}</h1>
        <div class="objetivos">
            @foreach($objetivos as $objetivo)
            <div class="objetivo" id="{{ $objetivo->id }}">
                <img src="{{ asset('assets/img/objetivos/'.$objetivo->imagem) }}" class="img-objetivo" alt="{{ $objetivo->{trans('banco.titulo')} }}">
                <p class="titulo-objetivo">{{ $objetivo->{trans('banco.titulo')} }}</p>
            </div>
            @endforeach
        </div>
    </div>
    <div class="saiba-mais">
        <a href="{{ route('a-abint') }}" class="saiba-mais-abint">{{ trans('frontend.home.saiba-mais') }} <img src="{{ asset('assets/img/layout/seta-banners-home.svg') }}" alt="Saiba mais" class="img-saiba-mais"></a>
    </div>

    <div class="informacao">
        <div class="comites">
            <hr class="linha-comites">
            @foreach($comites as $comite)
            <a href="{{ route('a-abint').'#abintComitesTecnicos' }}" class="comite"><img src="{{ asset('assets/img/comites/'.$comite->imagem) }}" class="img-comite" alt="{{ $comite->{trans('banco.nome')} }}"></a>
            @endforeach
        </div>
        <div class="itens-conteudo">
            <hr class="linha-itens">
            <a href="{{ route('naotecidos.index', ['submenu' => 'o-que-sao']) }}" class="item-conteudo" id="oQueSaoNT"><img src="{{ asset('assets/img/layout/bg-home-naotecidos.png') }}" alt="{{ trans('frontend.home.naotecidos') }}" class="img-conteudo"><span class="titulo">{{ trans('frontend.home.naotecidos') }}</span></a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'o-que-sao']) }}" class="item-conteudo" id="oQueSaoTT"><img src="{{ asset('assets/img/layout/bg-home-tecidostecnicos.png') }}" alt="{{ trans('frontend.home.tecidos-tecnicos') }}" class="img-conteudo"><span class="titulo">{{ trans('frontend.home.tecidos-tecnicos') }}</span></a>
            <a href="{{ route('informacao.index', ['submenu' => 'publicacoes']) }}" class="item-conteudo" id="publicacoes"><img src="{{ asset('assets/img/layout/bg-home-publicacoes.png') }}" alt="{{ trans('frontend.home.publicacoes') }}" class="img-conteudo"><span class="titulo">{{ trans('frontend.home.publicacoes') }}</span></a>
        </div>
    </div>

    @include('frontend.associados-link')



</div>


@endsection