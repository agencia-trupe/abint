@extends('frontend.common.template')

@section('content')

<div class="pg-associados">
    <h1 class="titulo">{{ trans('frontend.associados.titulo') }}</h1>

    <div class="maquinas-equipamentos" id="grupo-{{$grupos[0]->id}}">
        <h2 class="titulo-grupo">{{ $grupos[0]->{trans('banco.titulo')} }}</h2>
        <div class="associados">
            @foreach($associados as $associado)
            @if(in_array("Máquinas e Equipamentos", explode('|', $associado->grupo)) || in_array("Machines and Equipment", explode('|', $associado->grupo)))
            <a href="{{ $associado->link }}" target="_blank" class="associado" id="{{ $associado->id }}">
                <img src="{{ asset('assets/img/associados/'.$associado->imagem) }}" class="img-associado" alt="{{ $associado->nome }}" title="{{ $associado->nome }}">
            </a>
            @endif
            @endforeach
        </div>
    </div>

    <div class="materias-primas-insumos" id="grupo-{{$grupos[1]->id}}">
        <h2 class="titulo-grupo">{{ $grupos[1]->{trans('banco.titulo')} }}</h2>
        <div class="associados">
            @foreach($associados as $associado)
            @if(in_array("Matérias primas e Insumos", explode('|', $associado->grupo)) || in_array("Raw materials and Inputs", explode('|', $associado->grupo)))
            <a href="{{ $associado->link }}" target="_blank" class="associado" id="{{ $associado->id }}">
                <img src="{{ asset('assets/img/associados/'.$associado->imagem) }}" class="img-associado" alt="{{ $associado->nome }}" title="{{ $associado->nome }}">
            </a>
            @endif
            @endforeach
        </div>
    </div>

    <div class="fabricantes-naotecidos" id="grupo-{{$grupos[2]->id}}">
        <h2 class="titulo-grupo">{{ $grupos[2]->{trans('banco.titulo')} }}</h2>
        <div class="associados">
            @foreach($associados as $associado)
            @if(in_array("Fabricantes de Nãotecidos", explode('|', $associado->grupo)) || in_array("Nonwovens Manufacturers", explode('|', $associado->grupo)))
            <a href="{{ $associado->link }}" target="_blank" class="associado" id="{{ $associado->id }}">
                <img src="{{ asset('assets/img/associados/'.$associado->imagem) }}" class="img-associado" alt="{{ $associado->nome }}" title="{{ $associado->nome }}">
            </a>
            @endif
            @endforeach
        </div>
    </div>

    <div class="fabricantes-texteis-tecnicos" id="grupo-{{$grupos[3]->id}}">
        <h2 class="titulo-grupo">{{ $grupos[3]->{trans('banco.titulo')} }}</h2>
        <div class="associados">
            @foreach($associados as $associado)
            @if(in_array("Fabricantes de Tecidos e/ou Têxteis Técnicos", explode('|', $associado->grupo)) || in_array("Technical Fabric and/or Textile Manufacturers", explode('|', $associado->grupo)))
            <a href="{{ $associado->link }}" target="_blank" class="associado" id="{{ $associado->id }}">
                <img src="{{ asset('assets/img/associados/'.$associado->imagem) }}" class="img-associado" alt="{{ $associado->nome }}" title="{{ $associado->nome }}">
            </a>
            @endif
            @endforeach
        </div>
    </div>

    <div class="convertedores-distribuidores" id="grupo-{{$grupos[4]->id}}">
        <h2 class="titulo-grupo">{{ $grupos[4]->{trans('banco.titulo')} }}</h2>
        <div class="associados">
            @foreach($associados as $associado)
            @if(in_array("Convertedores e Distribuidores", explode('|', $associado->grupo)) || in_array("Converters and Distributors", explode('|', $associado->grupo)))
            <a href="{{ $associado->link }}" target="_blank" class="associado" id="{{ $associado->id }}">
                <img src="{{ asset('assets/img/associados/'.$associado->imagem) }}" class="img-associado" alt="{{ $associado->nome }}" title="{{ $associado->nome }}">
            </a>
            @endif
            @endforeach
        </div>
    </div>

    @include('frontend.associados-link')

</div>

@endsection