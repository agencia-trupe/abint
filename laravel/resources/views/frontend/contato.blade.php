@extends('frontend.common.template')

@section('content')

<div class="pag-contato">

    <h1 class="titulo">{{ trans('frontend.contato.titulo') }}</h1>

    <div class="contato">
        <div class="infos">
            @php
            $telefone1 = "+55".str_replace(" ", "", $contato->telefone_1);
            $telefone2 = "+55".str_replace(" ", "", $contato->telefone_2);
            @endphp
            <a href="tel:{{ $telefone1 }}" class="telefone">+55 {{ $contato->telefone_1 }}</a>
            <a href="tel:{{ $telefone2 }}" class="telefone">+55 {{ $contato->telefone_2 }}</a>
            <p class="endereco">{{ $contato->{trans('banco.endereco')} }} • {{ trans('frontend.contato.pais') }}</p>
        </div>

        <form action="{{ route('contato.post') }}" method="POST">
            {!! csrf_field() !!}
            <div class="dados">
                <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="{{ trans('frontend.contato.email') }}" value="{{ old('email') }}" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="{{ trans('frontend.contato.telefone') }}">
            </div>
            <textarea name="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required>{{ old('mensagem') }}</textarea>
            <button type="submit" class="btn-contato"><img src="{{ asset('assets/img/layout/seta-contato.svg') }}" alt="" class="img-enviar"></button>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>{{ trans('frontend.contato.sucesso') }}</p>
                <a href="{{ route('contato') }}">« {{ trans('frontend.contato.voltar') }}</a>
            </div>
            @endif
        </form>
    </div>

    <div class="contato-mapa">
        {!! $contato->google_maps !!}
    </div>

    @include('frontend.associados-link')

</div>

@endsection