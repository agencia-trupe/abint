@extends('frontend.common.template')

@section('content')

<div class="resultados-busca">

    <h1 class="titulo">{{ trans('frontend.busca.titulo') }}</h1>

    @if(count($resultados))
    <div class="resultados">
        @foreach($resultados as $key => $secoes)
        @if($key == 'naotecidos')
        @foreach($secoes as $resultado)
        <div class="resultado">
            <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado link-result-naotecidos" id="{{ $resultado['storageId'] }}">
                <h2 class="titulo">{{ $resultado['secao'] }} • {{ $resultado['titulo'] }}</h2>
                <div class="texto">{!! Tools::str_words(strip_tags($resultado['texto']), 30) !!}</div>
            </a>
        </div>
        @endforeach
        @elseif($key == 'texteis-tecnicos')
        @foreach($secoes as $resultado)
        <div class="resultado">
            <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado link-result-texteis-tecnicos" id="{{ $resultado['storageId'] }}">
                <h2 class="titulo">{{ $resultado['secao'] }} • {{ $resultado['titulo'] }}</h2>
                <div class="texto">{!! Tools::str_words(strip_tags($resultado['texto']), 30) !!}</div>
            </a>
        </div>
        @endforeach
        @elseif($key == 'informacao')
        @foreach($secoes as $resultado)
        <div class="resultado">
            <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado link-result-informacao" id="{{ $resultado['storageId'] }}">
                <h2 class="titulo">{{ $resultado['secao'] }} • {{ $resultado['titulo'] }}</h2>
                <div class="texto">{!! Tools::str_words(strip_tags($resultado['texto']), 30) !!}</div>
            </a>
        </div>
        @endforeach
        @else
        @foreach($secoes as $resultado)
        <div class="resultado">
            <a href="{{ $resultado['link'] }}" title="{{ $resultado['secao'] . ' - ' . $resultado['titulo'] }}" class="link-resultado">
                <h2 class="titulo">{{ $resultado['secao'] }} • {{ $resultado['titulo'] }}</h2>
                <div class="texto">{!! Tools::str_words(strip_tags($resultado['texto']), 30) !!}</div>
            </a>
        </div>
        @endforeach
        @endif
        @endforeach
    </div>
    @else
    <div class="nenhum-resultado">
        {{ trans('frontend.busca.nenhum') }}
    </div>
    @endif

    @include('frontend.associados-link')

</div>

@endsection