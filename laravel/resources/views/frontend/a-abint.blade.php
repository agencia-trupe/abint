@extends('frontend.common.template')

@section('content')

<div class="institucional">
    <div class="a-abint" id="aAbint">
        <img src="{{ asset('assets/img/institucional/'.$abint->imagem) }}" class="img-abint" alt="{{ trans('frontend.abint.titulo') }}">
        <div class="sobre">
            <h1 class="titulo">{{ trans('frontend.abint.titulo') }}</h1>
            {!! $abint->{trans('banco.texto')} !!}
        </div>
    </div>

    <div class="diretoria" id="abintDiretoria">
        <div class="centralizado">
            <div class="parte-um">
                <h1 class="titulo">{{ trans('frontend.abint.diretoria') }}</h1>
                <p class="periodo">{{ $abint->{trans('banco.bienio_periodo')} }}</p>
                <h4 class="cargo">{{ trans('frontend.abint.presidente') }}</h4>
                <div class="presidente" id="{{ $presidente->id }}">
                    <img src="{{ asset('assets/img/layout/icone-comites.svg') }}" alt="" class="img-diretoria">
                    <div class="dados">
                        <p class="nome">{{ $presidente->nome }}</p>
                        <p class="empresa">{{ $presidente->empresa }}</p>
                    </div>
                </div>
                <h4 class="cargo vice">{{ trans('frontend.abint.vice') }}</h4>
                <div class="vice-presidente" id="{{ $vicePresidente->id }}">
                    <img src="{{ asset('assets/img/layout/icone-comites.svg') }}" alt="" class="img-diretoria">
                    <div class="dados">
                        <p class="nome">{{ $vicePresidente->nome }}</p>
                        <p class="empresa">{{ $vicePresidente->empresa }}</p>
                    </div>
                </div>
                <h4 class="cargo conselheiro">{{ trans('frontend.abint.conselheiros') }}</h4>
                @if($conselheiros)
                @foreach($conselheiros as $conselheiro)
                <div class="conselheiro" id="{{ $conselheiro->id }}">
                    <img src="{{ asset('assets/img/layout/icone-comites.svg') }}" alt="" class="img-diretoria">
                    <div class="dados">
                        <p class="nome">{{ $conselheiro->nome }}</p>
                        <p class="empresa">{{ $conselheiro->empresa }}</p>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <div class="parte-dois">
                <h4 class="cargo">{{ trans('frontend.abint.diretores') }}</h4>
                @foreach($diretores as $diretor)
                <div class="diretor" id="{{ $diretor->id }}">
                    <img src="{{ asset('assets/img/layout/icone-comites.svg') }}" alt="" class="img-diretoria">
                    <div class="dados">
                        <p class="nome">{{ $diretor->nome }}</p>
                        <p class="empresa">{{ $diretor->empresa }}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="comites" id="abintComitesTecnicos">
        <div class="centralizado">
            <h1 class="titulo">{{ trans('frontend.abint.comites') }}</h1>
            @foreach($comites as $comite)
            <div class="comite" id="{{ $comite->id }}">
                <div class="img-dados">
                    <img src="{{ asset('assets/img/comites/'.$comite->imagem) }}" class="img-comite" alt="{{ $comite->{trans('banco.nome')} }}">
                    <div class="dados">
                        <h4 class="nome">{{ $comite->{trans('banco.nome')} }}</h4>
                        <a href="{{ $comite->website }}" target="_blank" class="website">{{ str_replace(["https://", "http://", "/"], "", $comite->website) }}</a>
                        <div class="social">
                            <a href="{{ $comite->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="Facebook" class="img-social"></a>
                            <a href="{{ $comite->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="Instagram" class="img-social"></a>
                            <a href="{{ $comite->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt="LinkedIn" class="img-social"></a>
                        </div>
                    </div>
                </div>
                <div class="membros">
                    @foreach($associados as $membro)
                    @if($membro->comite_id == $comite->id)
                    <a href="{{ $membro->link }}" target="_blank" class="membro">
                        <img src="{{ asset('assets/img/associados/'.$membro->imagem) }}" class="img-membro" alt="{{ $membro->nome }}" title="{{ $membro->nome }}">
                    </a>
                    @endif
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>

    @include('frontend.associados-link')

</div>


@endsection