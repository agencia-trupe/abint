@extends('frontend.common.template')

@section('content')

<div class="informacao">
    <!-- troca de submenus via AJAX -->
    <h1 class="titulo">{{ trans('frontend.informacao.titulo') }}</h1>
    <div class="centralizado">
        <div class="submenu">
            <a href="{{ route('informacao.getDadosInformacao', ['submenu' => 'publicacoes']) }}" class="link active" id="publicacoes">» {{ trans('frontend.informacao.publicacoes') }}</a>
            <a href="{{ route('informacao.getDadosInformacao', ['submenu' => 'cursos-e-eventos']) }}" class="link" id="cursosEEventos">» {{ trans('frontend.informacao.cursos-e-eventos') }}</a>
            <a href="{{ route('informacao.getDadosInformacao', ['submenu' => 'normas-e-ensaios']) }}" class="link" id="normasEEnsaios">» {{ trans('frontend.informacao.normas-e-ensaios') }}</a>
            <a href="{{ route('informacao.getDadosInformacao', ['submenu' => 'imprensa']) }}" class="link" id="imprensa">» {{ trans('frontend.informacao.imprensa') }}</a>
            <a href="{{ route('informacao.getDadosInformacao', ['submenu' => 'outros']) }}" class="link" id="outros">» {{ trans('frontend.informacao.outros') }}</a>
        </div>
        <div class="conteudo" id="conteudoInformacao">
            <h2 class="titulo">{{ $titulo }}</h2>

            @if($publicacoes)
            @foreach($publicacoes as $publicacao)
            <a href="{{ route('publicacoes.download', $publicacao->slug) }}" target="_blank" class="publicacao" id="{{ $publicacao->id }}">
                <img src="{{ asset('assets/img/publicacoes/'.$publicacao->imagem) }}" class="img-publicacao" alt="{{ $publicacao->{trans('banco.titulo')} }}">
                <div class="dados">
                    <h4 class="titulo">{{ $publicacao->{trans('banco.titulo')} }}</h4>
                    <div class="descricao">{!! $publicacao->{trans('banco.descricao')} !!}</div>
                </div>
                <img src="{{ asset('assets/img/layout/ico-pdf.svg') }}" alt="" class="img-download">
            </a>
            @endforeach
            @endif

            @if($cursosEventos)
            @foreach($cursosEventos as $evento)
            <div class="curso-evento" id="{{$evento->id}}">
                <a href="{{ asset('assets/img/eventos/originais/'.$evento->imagem) }}" class="fancybox">
                    <img src="{{ asset('assets/img/eventos/'.$evento->imagem) }}" class='img-curso-evento' alt="{{ $evento->{trans('banco.nome')} }}">
                </a>
                <div class="dados">
                    <h4 class="titulo">{{ $evento->{trans('banco.nome')} }}</h4>
                    <p class="periodo">{{ $evento->{trans('banco.periodo')} }}</p>
                    <div class="descricao">{!! $evento->{trans('banco.descricao')} !!}</div>
                    <a href="{{ $evento->link }}" target="_blank" class="link-texto">{{ $evento->link }}</a>
                </div>
                <a href="{{ $evento->link }}" target="_blank" class="icones">
                    <img src="{{ asset('assets/img/layout/ico-visitarsite-laranja.svg') }}" class="img-visitar" alt="{{ $evento->link }}">
                    <img src="{{ asset('assets/img/layout/icone-linkseta.svg') }}" class="img-seta" alt="{{ $evento->link }}">
                </a>
            </div>
            @endforeach
            @endif

            @if($normasEnsaios)
            @foreach($normasEnsaios as $norma)
            <a href="{{ route('normas-e-ensaios.download', $norma->slug) }}" target="_blank" class="norma-ensaio" id="{{ $norma->id }}">
                <img src="{{ asset('assets/img/normas-e-ensaios/'.$norma->imagem) }}" class="img-norma-ensaio" alt="{{ $norma->{trans('banco.titulo')} }}">
                <div class="dados">
                    <h4 class="titulo">{{ $norma->{trans('banco.titulo')} }}</h4>
                    <div class="descricao">{!! $norma->{trans('banco.descricao')} !!}</div>
                </div>
                <img src="{{ asset('assets/img/layout/ico-pdf.svg') }}" class="img-download" alt="">
            </a>
            @endforeach
            @endif

            @if($novidades)
            @foreach($novidades as $novidade)
            <a href="{{ route('novidades.show', $novidade->slug) }}" class="novidade" id="imprensa">
                <img src="{{ asset('assets/img/novidades/lista/'.$novidade->imagem) }}" class="img-novidade" alt="{{ $novidade->{trans('banco.titulo')} }}">
                <div class="dados">
                    <p class="data">{{ strftime('%d %b %Y', strtotime($novidade->data)) }}</p>
                    <h4 class="titulo">{{ $novidade->{trans('banco.titulo')} }}</h4>
                </div>
                <div class="ler-mais">
                    <div class="triangulo"></div>
                    <div class="retangulo">{{ trans('frontend.informacao.ler-mais') }} » <img src="{{ asset('assets/img/layout/icone-linkseta-branco.svg') }}" alt="" class="img-site"></div>
                </div>
            </a>
            @endforeach
            @endif

            @if($outros)
            @foreach($outros as $outro)
            <a href="{{ route('outros.show', $outro->slug) }}" class="outro" id="outros">
                <img src="{{ asset('assets/img/outros/lista/'.$outro->imagem) }}" class="img-outro" alt="{{ $outro->{trans('banco.titulo')} }}">
                <div class="dados">
                    <p class="data">{{ strftime("%d %b %Y", strtotime($outro->data)) }}</p>
                    <h4 class="titulo">{{ $outro->{trans('banco.titulo')} }}</h4>
                </div>
                <div class="ler-mais">
                    <div class="triangulo"></div>
                    <div class="retangulo">{{ trans('frontend.informacao.ler-mais') }} » <img src="{{ asset('assets/img/layout/icone-linkseta-branco.svg') }}" alt="" class="img-site"></div>
                </div>
            </a>
            @endforeach
            @endif
        </div>
    </div>

    @include('frontend.associados-link')

</div>

@endsection