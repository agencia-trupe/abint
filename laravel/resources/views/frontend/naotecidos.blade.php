@extends('frontend.common.template')

@section('content')

<div class="naotecidos">
    <h1 class="titulo">{{ trans('frontend.naotecidos.titulo') }}</h1>
    <div class="centralizado">
        <div class="submenu">
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'o-que-sao']) }}" class="link active" id="oQueSaoNT">» {{ trans('frontend.naotecidos.o-que-sao') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'historia']) }}" class="link" id="historiaNT">» {{ trans('frontend.naotecidos.historia') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'materias-primas']) }}" class="link" id="matPrimaNT">» {{ trans('frontend.naotecidos.mat-primas') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'como-sao-fabricados']) }}" class="link" id="comoSaoFabNT">» {{ trans('frontend.naotecidos.fabricacao') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'transf-acabamento-conversao']) }}" class="link" id="transAcabConv">» {{ trans('frontend.naotecidos.transformacao') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'classificacao']) }}" class="link" id="classificacaoNT">» {{ trans('frontend.naotecidos.classificacao') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'identificacao']) }}" class="link" id="idenfiticacaoNT">» {{ trans('frontend.naotecidos.identificacao') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'algumas-aplicacoes']) }}" class="link" id="algumasAplicacoesNT">» {{ trans('frontend.naotecidos.algumas-aplicacoes') }}</a>
            <a href="{{ route('naotecidos.getDados', ['submenu' => 'aplicacoes-a-z']) }}" class="link" id="aplicacoesAZ">» {{ trans('frontend.naotecidos.aplicacoes-a-z') }}</a>
        </div>
        <div class="conteudo" id="conteudoNaoTecidos">
            <!-- troca de submenus via AJAX -->
            @if($algumasAplicacoes)
            @foreach($algumasAplicacoes as $aplicacao)
            <div class="alguma-aplicacao">
                <img src="{{ asset('assets/img/naotecidos-aplicacoes/'.$aplicacao->imagem) }}" class="img-aplicacao" alt="{{ $aplicacao->{trans('banco.titulo')} }}">
                <div class="dados-aplicacao">
                    <h4 class="titulo">{{ $aplicacao->{trans('banco.titulo')} }}</h4>
                    <div class='texto-aplicacao'>{!! $aplicacao->{trans('banco.texto')} !!}</div>
                </div>
            </div>
            @endforeach
            @elseif($aplicacoesAZ)
            @foreach($aplicacoesAZ as $aplicacao)
            <div class="aplicacoes-a-z">
                <p class="frase">{{ trans('frontend.naotecidos.frase-aplicacoes-a-z') }}</p>
                <div class='letras' id='listaLetras'>
                    @foreach($letras as $letra)
                    <a href="#" class="letra-aplicacao" id="{{ $letra }}">{{ $letra }}</a>
                    @endforeach
                </div>
                <div class='resultados' id='resultadosLetra'>
                    <!-- ajax -->
                </div>
            </div>
            @endforeach
            @else
            <h2 class="titulo">{{ $titulo }}</h2>
            <div class="texto">{!! $resultado->{trans('banco.texto')} !!}</div>
            @endif
        </div>
    </div>

    @include('frontend.associados-link')

</div>

@endsection