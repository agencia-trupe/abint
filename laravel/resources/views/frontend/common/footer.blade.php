<footer>
    <div class="centralizado">
        <div class="parte-um">
            <a href="{{ route('home') }}" class="logo-footer" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-abint.svg') }}" alt="{{ $config->title }}" class="img-logo"></a>
            <div class="social">
                <a href="{{ $contato->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="Facebook"></a>
                <a href="{{ $contato->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="Instagram"></a>
                <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt="Linkedin"></a>
            </div>
        </div>

        <div class="parte-dois">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="nav-footer link-home active" @endif class="nav-footer link-home">{{ trans('frontend.header.home') }}</a>

            <a href="{{ route('a-abint') }}" @if(Tools::routeIs('a-abint')) class="nav-footer active" @endif class="nav-footer">{{ trans('frontend.header.a-abint') }}</a>
            <a href="{{ route('a-abint').'#aAbint' }}" class="submenu-footer">{{ trans('frontend.header.a-abint') }}</a>
            <a href="{{ route('a-abint').'#abintDiretoria' }}" class="submenu-footer">{{ trans('frontend.footer.diretoria') }}</a>
            <a href="{{ route('a-abint').'#abintComitesTecnicos' }}" class="submenu-footer">{{ trans('frontend.footer.comites') }}</a>

            <a href="{{ route('associados') }}" @if(Tools::routeIs('associados')) class="nav-footer link-associados active" @endif class="nav-footer link-associados">{{ trans('frontend.header.associados') }}</a>
            @foreach($grupos as $grupo)
            <a href="{{ route('associados').'#grupo-'.$grupo->id }}" class="submenu-footer">{{ $grupo->{trans('banco.titulo')} }}</a>
            @endforeach
        </div>

        <div class="parte-tres">
            <a href="{{ route('naotecidos.index', ['submenu' => 'o-que-sao']) }}" @if(Tools::routeIs('naotecidos*')) class="nav-footer active" @endif class="nav-footer" id="oQueSaoNT">{{ trans('frontend.header.naotecidos') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'o-que-sao']) }}" class="submenu-footer-naotecidos" id="oQueSaoNT">{{ trans('frontend.naotecidos.o-que-sao') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'historia']) }}" class="submenu-footer-naotecidos" id="historiaNT">{{ trans('frontend.naotecidos.historia') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'materias-primas']) }}" class="submenu-footer-naotecidos" id="matPrimaNT">{{ trans('frontend.naotecidos.mat-primas') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'como-sao-fabricados']) }}" class="submenu-footer-naotecidos" id="comoSaoFabNT">{{ trans('frontend.naotecidos.fabricacao') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'transf-acabamento-conversao']) }}" class="submenu-footer-naotecidos" id="transAcabConv">{{ trans('frontend.naotecidos.transformacao') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'classificacao']) }}" class="submenu-footer-naotecidos" id="classificacaoNT">{{ trans('frontend.naotecidos.classificacao') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'identificacao']) }}" class="submenu-footer-naotecidos" id="idenfiticacaoNT">{{ trans('frontend.naotecidos.identificacao') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'algumas-aplicacoes']) }}" class="submenu-footer-naotecidos" id="algumasAplicacoesNT">{{ trans('frontend.naotecidos.algumas-aplicacoes') }}</a>
            <a href="{{ route('naotecidos.index', ['submenu' => 'aplicacoes-a-z']) }}" class="submenu-footer-naotecidos" id="aplicacoesAZ">{{ trans('frontend.naotecidos.aplicacoes-a-z') }}</a>

            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'o-que-sao']) }}" @if(Tools::routeIs('texteis-tecnicos*')) class="nav-footer link-txt-tec active" @endif class="nav-footer link-txt-tec" id="oQueSaoTT">{{ trans('frontend.header.texteis-tecnicos') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'o-que-sao']) }}" class="submenu-footer-texteis-tecnicos" id="oQueSaoTT">{{ trans('frontend.texteis-tec.o-que-sao') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'materias-primas']) }}" class="submenu-footer-texteis-tecnicos" id="matPrimaTT">{{ trans('frontend.texteis-tec.mat-primas') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'como-sao-fabricados']) }}" class="submenu-footer-texteis-tecnicos" id="comoSaoFabTT">{{ trans('frontend.texteis-tec.fabricacao') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'processo-transf-acabamento']) }}" class="submenu-footer-texteis-tecnicos" id="processoTransfAcab">{{ trans('frontend.texteis-tec.processos') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'classificacao']) }}" class="submenu-footer-texteis-tecnicos" id="classificacaoTT">{{ trans('frontend.texteis-tec.classificacao') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'desempenho']) }}" class="submenu-footer-texteis-tecnicos" id="desempenhoTT">{{ trans('frontend.texteis-tec.desempenho') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'identificacao']) }}" class="submenu-footer-texteis-tecnicos" id="identificacaoTT">{{ trans('frontend.texteis-tec.identificacao') }}</a>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'algumas-aplicacoes']) }}" class="submenu-footer-texteis-tecnicos" id="algumasAplicacoesTT">{{ trans('frontend.texteis-tec.aplicacoes') }}</a>
        </div>

        <div class="parte-quatro">
            <a href="{{ route('informacao.index', ['submenu' => 'publicacoes']) }}" @if(Tools::routeIs('informacao*')) class="nav-footer active" @endif class="nav-footer" id="publicacoes">{{ trans('frontend.header.informacao') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'publicacoes']) }}" class="submenu-footer-informacao" id="publicacoes">{{ trans('frontend.header.publicacoes') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'cursos-e-eventos']) }}" class="submenu-footer-informacao" id="cursosEEventos">{{ trans('frontend.header.cursos-eventos') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'normas-e-ensaios']) }}" class="submenu-footer-informacao" id="normasEEnsaios">{{ trans('frontend.header.normas-ensaios') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'imprensa']) }}" class="submenu-footer-informacao" id="imprensa">{{ trans('frontend.header.imprensa') }}</a>
            <a href="{{ route('informacao.index', ['submenu' => 'outros']) }}" class="submenu-footer-informacao" id="outros">{{ trans('frontend.header.outros') }}</a>

            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="nav-footer link-contato active" @endif class="nav-footer link-contato">{{ trans('frontend.header.contato') }}</a>
            @php
            $telefone1 = str_replace(" ", "", $contato->telefone_1);
            $telefone2 = str_replace(" ", "", $contato->telefone_2);
            @endphp
            <a href="tel:{{ $telefone1 }}" class="telefone">{{ $contato->telefone_1 }}</a>
            <a href="tel:{{ $telefone2 }}" class="telefone">{{ $contato->telefone_2 }}</a>
            <a href="mailto:{{ $contato->email }}" target="_blank" class="email">{{ $contato->email }}</a>
            <p class="endereco">{{ $contato->{trans('banco.endereco')} }}</p>

            <a href="{{ route('politica-de-privacidade') }}" @if(Tools::routeIs('politica-de-privacidade')) class="submenu-footer active" @endif class="submenu-footer">{{ trans('frontend.footer.politica') }}</a>
        </div>
    </div>
    <hr class="linha-footer">
    <div class="direitos">
        <p class="empresa">© {{ date('Y') }} <span>{{ $config->title }} {{ $config->{trans('banco.description')} }}</span> &middot; {{ trans('frontend.footer.direitos') }}</p>
        <p class="trupe">{{ trans('frontend.footer.criacao-sites') }} <a href="http://www.trupe.net" target="_blank" class="link-trupe">Trupe Agência Criativa</a></p>
    </div>
</footer>