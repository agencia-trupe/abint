<div class="itens-menu">

    <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="nav-link active" @endif class="nav-link"><span class="link-hover">|</span>{{ trans('frontend.header.home') }}</a>
    <a href="{{ route('a-abint') }}" @if(Tools::routeIs('a-abint')) class="nav-link active" @endif class="nav-link" id="menuAAbint" submenu="submenu-a-abint"><span class="link-hover">|</span>{{ trans('frontend.header.a-abint') }}</a>

    <a href="{{ route('associados') }}" @if(Tools::routeIs('associados')) class="nav-link active" @endif class="nav-link" id="menuAssociados" submenu="submenu-associados">
        <span class="link-hover">|</span>{{ trans('frontend.header.associados') }}
    </a>

    <a href="{{ route('naotecidos.index', ['submenu' => 'o-que-sao']) }}" @if(Tools::routeIs('naotecidos*')) class="nav-link active" @endif class="nav-link" id="oQueSaoNT" submenu="submenu-naotecidos"><span class="link-hover">|</span>{{ trans('frontend.header.naotecidos') }}</a>
    <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'o-que-sao']) }}" @if(Tools::routeIs('texteis-tecnicos*')) class="nav-link active" @endif class="nav-link" id="oQueSaoTT" submenu="submenu-texteis-tecnicos"><span class="link-hover">|</span>{{ trans('frontend.header.texteis-tecnicos') }}</a>
    <a href="{{ route('informacao.index', ['submenu' => 'publicacoes']) }}" @if(Tools::routeIs('informacao*')) class="nav-link active" @endif class="nav-link" id="publicacoes" submenu="submenu-informacao"><span class="link-hover">|</span>{{ trans('frontend.header.informacao') }}</a>

    <a href="{{ route('associe-se') }}" @if(Tools::routeIs('associe-se')) class="nav-link active" @endif class="nav-link" id="menuAssocieSe" submenu="submenu-associe-se">
        <span class="link-hover">|</span>{{ trans('frontend.header.associe-se') }}
    </a>

    <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="nav-link active" @endif class="nav-link"><span class="link-hover">|</span>{{ trans('frontend.header.contato') }}</a>

    <div class="social">
        <a href="{{ $contato->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="Facebook" class="img-social"></a>
        <a href="{{ $contato->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="Instagram" class="img-social"></a>
        <a href="{{ $contato->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt="LinkedIn" class="img-social"></a>
    </div>

    <a href="#" class="lupa-header"><img src="{{ asset('assets/img/layout/ico-lupa.svg') }}" alt="Buscar" class="img-lupa"></a>

    <form action="{{ route('busca.post') }}" method="POST" class="campo-busca" style="display: none">
        {!! csrf_field() !!}
        <input type="text" name="busca" placeholder="{{ trans('frontend.header.buscar') }}">
        <button type="submit" class="lupa-campo"><img src="{{ asset('assets/img/layout/ico-lupa-cinza.svg') }}" alt="Buscar" class="img-campo-lupa"></button>
    </form>

    <div class="idiomas" style="display: none;">
        @foreach([
        'pt' => 'PT',
        'en' => 'EN',
        ] as $key => $title)
        @if(app()->getLocale() == $key)
        <a class="idiomas-titulo" idioma="{{ $key }}">{{ $title }} <img src="{{ asset('assets/img/layout/seta-idiomas.svg') }}" alt="" class="img-idiomas"></a>
        @endif
        <div class="idiomas-links">
            @unless(app()->getLocale() == $key)
            <a href="{{ route('lang', $key) }}" class="link">{{ $title }}</a>
            @endunless
        </div>
        @endforeach
    </div>

    <!-- SUBMENUS A ABINT -->
    <ul class="submenu-a-abint submenu-header" style="display: none;">
        <li>
            <a href="{{ route('a-abint').'#aAbint' }}" class="submenu-link">{{ trans('frontend.header.a-abint') }}</a>
        </li>
        <li>
            <a href="{{ route('a-abint').'#abintDiretoria' }}" class="submenu-link">{{ trans('frontend.footer.diretoria') }}</a>
        </li>
        <li>
            <a href="{{ route('a-abint').'#abintComitesTecnicos' }}" class="submenu-link">{{ trans('frontend.footer.comites') }}</a>
        </li>
    </ul>

    <!-- SUBMENUS ASSOCIADOS -->
    <ul class="submenu-associados submenu-header" style="display: none;">
        @foreach($grupos as $grupo)
        <li>
            <a href="{{ route('associados').'#grupo-'.$grupo->id }}" class="submenu-link">{{ $grupo->{trans('banco.titulo')} }}</a>
        </li>
        @endforeach
    </ul>

    <!-- SUBMENUS NÃOTECIDOS -->
    <ul class="submenu-naotecidos submenu-header" style="display: none;">
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'o-que-sao']) }}" class="submenu-link" id="oQueSaoNT">{{ trans('frontend.naotecidos.o-que-sao') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'historia']) }}" class="submenu-link" id="historiaNT">{{ trans('frontend.naotecidos.historia') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'materias-primas']) }}" class="submenu-link" id="matPrimaNT">{{ trans('frontend.naotecidos.mat-primas') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'como-sao-fabricados']) }}" class="submenu-link" id="comoSaoFabNT">{{ trans('frontend.naotecidos.fabricacao') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'transf-acabamento-conversao']) }}" class="submenu-link" id="transAcabConv">{{ trans('frontend.naotecidos.transformacao') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'classificacao']) }}" class="submenu-link" id="classificacaoNT">{{ trans('frontend.naotecidos.classificacao') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'identificacao']) }}" class="submenu-link" id="idenfiticacaoNT">{{ trans('frontend.naotecidos.identificacao') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'algumas-aplicacoes']) }}" class="submenu-link" id="algumasAplicacoesNT">{{ trans('frontend.naotecidos.algumas-aplicacoes') }}</a>
        </li>
        <li>
            <a href="{{ route('naotecidos.index', ['submenu' => 'aplicacoes-a-z']) }}" class="submenu-link" id="aplicacoesAZ">{{ trans('frontend.naotecidos.aplicacoes-a-z') }}</a>
        </li>
    </ul>

    <!-- SUBMENUS TÊXTEIS TÉCNICOS -->
    <ul class="submenu-texteis-tecnicos submenu-header" style="display: none;">
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'o-que-sao']) }}" class="submenu-link" id="oQueSaoTT">{{ trans('frontend.texteis-tec.o-que-sao') }}</a>
        </li>
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'materias-primas']) }}" class="submenu-link" id="matPrimaTT">{{ trans('frontend.texteis-tec.mat-primas') }}</a>
        </li>
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'como-sao-fabricados']) }}" class="submenu-link" id="comoSaoFabTT">{{ trans('frontend.texteis-tec.fabricacao') }}</a>
        </li>
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'processo-transf-acabamento']) }}" class="submenu-link" id="processoTransfAcab">{{ trans('frontend.texteis-tec.processos') }}</a>
        </li>
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'classificacao']) }}" class="submenu-link" id="classificacaoTT">{{ trans('frontend.texteis-tec.classificacao') }}</a>
        </li>
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'desempenho']) }}" class="submenu-link" id="desempenhoTT">{{ trans('frontend.texteis-tec.desempenho') }}</a>
        </li>
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'identificacao']) }}" class="submenu-link" id="identificacaoTT">{{ trans('frontend.texteis-tec.identificacao') }}</a>
        </li>
        <li>
            <a href="{{ route('texteis-tecnicos.index', ['submenu' => 'algumas-aplicacoes']) }}" class="submenu-link" id="algumasAplicacoesTT">{{ trans('frontend.texteis-tec.aplicacoes') }}</a>
        </li>
    </ul>

    <!-- SUBMENU INFORMAÇÃO -->
    <ul class="submenu-informacao submenu-header" style="display: none;">
        <li>
            <a href="{{ route('informacao.index', ['submenu' => 'publicacoes']) }}" class="submenu-link" id="publicacoes">{{ trans('frontend.header.publicacoes') }}</a>
        </li>
        <li>
            <a href="{{ route('informacao.index', ['submenu' => 'cursos-e-eventos']) }}" class="submenu-link" id="cursosEEventos">{{ trans('frontend.header.cursos-eventos') }}</a>
        </li>
        <li>
            <a href="{{ route('informacao.index', ['submenu' => 'normas-e-ensaios']) }}" class="submenu-link" id="normasEEnsaios">{{ trans('frontend.header.normas-ensaios') }}</a>
        </li>
        <li>
            <a href="{{ route('informacao.index', ['submenu' => 'imprensa']) }}" class="submenu-link" id="imprensa">{{ trans('frontend.header.imprensa') }}</a>
        </li>
        <li>
            <a href="{{ route('informacao.index', ['submenu' => 'outros']) }}" class="submenu-link" id="outros">{{ trans('frontend.header.outros') }}</a>
        </li>
    </ul>
</div>