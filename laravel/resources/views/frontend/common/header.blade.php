<header>
    <div class="bg">
        <img src="{{ asset('assets/img/layout/bg-cabecalho.svg') }}" alt="" class="bg-img">
    </div>
    <div class="header-center">
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
        <nav>
            @include('frontend.common.nav')
        </nav>
        @if(Tools::routeIs('home'))
        <a href="{{ route('home') }}" class="logo" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-abint.svg') }}" alt="{{ $config->title }}" class="img-logo"></a>
        @else
        <a href="{{ route('home') }}" class="logo" title="{{ $config->title }}"><img src="{{ asset('assets/img/layout/marca-abint2.svg') }}" alt="{{ $config->title }}" class="img-logo"></a>
        @endif
    </div>
</header>