@extends('frontend.common.template')

@section('content')

<div class="associe-se">
    <h1 class="titulo">{{ trans('frontend.associe-se.titulo') }}</h1>

    <div class="motivos">
        <h2 class="titulo-motivos">{{ $associe->{trans('banco.titulo_motivos')} }}</h2>
        <div class="itens">
            <div class="left">
                <h3 class="titulo1">{{ $associe->{trans('banco.m1_titulo')} }}</h3>
                <div class="texto1">{!! $associe->{trans('banco.m1_texto')} !!}</div>
                <h3 class="titulo2">{{ $associe->{trans('banco.m2_titulo')} }}</h3>
                <div class="texto2">{!! $associe->{trans('banco.m2_texto')} !!}</div>
            </div>
            <div class="right">
                <h3 class="titulo3">{{ $associe->{trans('banco.m3_titulo')} }}</h3>
                <div class="texto3">{!! $associe->{trans('banco.m3_texto')} !!}</div>
                <h3 class="titulo4">{{ $associe->{trans('banco.m4_titulo')} }}</h3>
                <div class="texto4">{!! $associe->{trans('banco.m4_texto')} !!}</div>
            </div>
        </div>
    </div>

    <div class="missao-visao-pilares">
        <div class="centralizado">
            <div class="missao">
                <div class="imagem-item">
                    <img src="{{ asset('assets/img/layout/icone-missao.svg') }}" alt="" class="img-icone">
                </div>
                <p class="titulo-item">{{ trans('frontend.associe-se.missao') }}</p>
                <div class="texto-item">{!! $associe->{trans('banco.missao')} !!}</div>
            </div>
            <div class="visao">
                <div class="imagem-item">
                    <img src="{{ asset('assets/img/layout/icone-visao.svg') }}" alt="" class="img-icone">
                </div>
                <p class="titulo-item">{{ trans('frontend.associe-se.visao') }}</p>
                <div class="texto-item">{!! $associe->{trans('banco.visao')} !!}</div>
            </div>
            <div class="pilares">
                <div class="imagem-item">
                    <img src="{{ asset('assets/img/layout/icone-pilares.svg') }}" alt="" class="img-icone">
                </div>
                <p class="titulo-item">{{ trans('frontend.associe-se.pilares') }}</p>
                <div class="texto-item">{!! $associe->{trans('banco.pilares')} !!}</div>
            </div>
        </div>
    </div>

    <div class="comites-tecnicos">
        <h2 class="titulo-comites">{{ trans('frontend.abint.comites') }}</h2>
        @foreach($comites as $comite)
        <div class="comite">
            <div class="capa-comite">
                <img src="{{ asset('assets/img/comites/'.$comite->imagem) }}" class="img-comite" alt="{{ $comite->{trans('banco.nome')} }}">
            </div>
            <div class="left">
                <h4 class="nome">{{ $comite->{trans('banco.nome')} }}</h4>
                <a href="{{ $comite->website }}" target="_blank" class="website">{{ str_replace(["https://", "http://", "/"], "", $comite->website) }}</a>
                <div class="social">
                    <a href="{{ $comite->facebook }}" target="_blank" class="facebook"><img src="{{ asset('assets/img/layout/ico-facebook.svg') }}" alt="Facebook" class="img-social"></a>
                    <a href="{{ $comite->instagram }}" target="_blank" class="instagram"><img src="{{ asset('assets/img/layout/ico-instagram.svg') }}" alt="Instagram" class="img-social"></a>
                    <a href="{{ $comite->linkedin }}" target="_blank" class="linkedin"><img src="{{ asset('assets/img/layout/ico-linkedin.svg') }}" alt="LinkedIn" class="img-social"></a>
                </div>
            </div>
            <div class="right">{!! $comite->{trans('banco.texto')} !!}</div>
        </div>
        @endforeach
    </div>

    <div class="form-associe-se">
        <div class="infos">
            <h1 class="titulo-form">{{ trans('frontend.associe-se.titulo') }}!</h1>
            <p class="frase-form">{{ trans('frontend.associe-se.frase') }}</p>
        </div>
        <form action="{{ route('associe-se.post') }}" method="post">
            {!! csrf_field() !!}
            <div class="dados">
                <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="{{ trans('frontend.contato.email') }}" value="{{ old('email') }}" required>
                <input type="text" name="telefone" value="{{ old('telefone') }}" placeholder="{{ trans('frontend.contato.telefone') }}">
            </div>
            <button type="submit" class="btn-enviar-associe-se">ENVIAR <img src="{{ asset('assets/img/layout/seta-contato.svg') }}" alt="" class="img-enviar"></button>
        </form>
        @if($errors->any())
        <div class="flash flash-erro">
            @foreach($errors->all() as $error)
            {!! $error !!}<br>
            @endforeach
        </div>
        @endif

        @if(session('enviado'))
        <div class="flash flash-sucesso">
            <p>{{ trans('frontend.contato.sucesso') }}</p>
        </div>
        @endif
    </div>

    @include('frontend.associados-link')

</div>

@endsection