<a href="{{ route('associados') }}" class="link-associados">
    <img src="{{ asset('assets/img/layout/bg-marcas-associadas.png') }}" alt="Associados" class="img-marcas">
    <p class="texto-link">{{ trans('frontend.associados.link-texto') }} <img src="{{ asset('assets/img/layout/seta-banners-home.svg') }}" alt="" class="img-seta"></p>
</a>