<!doctype html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/bootswatch-dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/painel.css') }}">
</head>

<body class="painel-login">

    <div class="col-md-4 col-sm-6 col-xs-10 login">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('password-reset.post') }}" style="width: 100%;">
            {{ csrf_field() }}
            <h3>
                {{ config('app.name') }}
                <small>Painel Administrativo</small>
            </h3>

            @if(session('error'))
            <div class="alert alert-danger">{{ session('error') }}</div>
            @endif

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group" style="position:relative; margin-left:0; margin-right:0;">
                <div class="input-group {{ $errors->has('email') ? ' has-error' : '' }}" style="width:100%;">
                    <label for="email" class="control-label">E-MAIL:</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="input-group {{ $errors->has('password') ? ' has-error' : '' }}" style="width:100%;">
                    <label for="password" class="control-label">SENHA:</label>
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="input-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" style="width:100%;">
                    <label for="password-confirm" class="control-label">REPETIR SENHA:</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <button type="submit" class="btn btn-success btn-block" style="margin-top:20px;">REDEFINIR SENHA</button>

        </form>
    </div>
</body>

</html>