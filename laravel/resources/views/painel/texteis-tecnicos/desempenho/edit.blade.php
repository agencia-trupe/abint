@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Desempenho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.texteis-tecnicos.desempenho.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.texteis-tecnicos.desempenho.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
