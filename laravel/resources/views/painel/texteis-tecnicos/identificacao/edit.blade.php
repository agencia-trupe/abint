@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Identificação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.texteis-tecnicos.identificacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.texteis-tecnicos.identificacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
