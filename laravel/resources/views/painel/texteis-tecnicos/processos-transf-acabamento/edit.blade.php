@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Processos de transformação e acabamento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.texteis-tecnicos.processos-transf-acabamento.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.texteis-tecnicos.processos-transf-acabamento.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
