@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Aplicações</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.texteis-tecnicos.aplicacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.texteis-tecnicos.aplicacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
