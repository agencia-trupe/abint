@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Classificação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.classificacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.classificacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
