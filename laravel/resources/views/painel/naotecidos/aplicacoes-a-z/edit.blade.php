@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aplicações de A a Z /</small> Editar Aplicação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.aplicacoes-a-z.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.aplicacoes-a-z.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
