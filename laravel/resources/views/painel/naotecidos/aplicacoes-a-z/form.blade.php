@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Titulo') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo_en', 'Titulo (INGLÊS)') !!}
    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('itens', 'Itens *SEPARAR POR | (pipe ou barra vertical, sem espaços)*') !!}
    {!! Form::textarea('itens', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
    <p class="exemplo-separacao"><strong>Exemplo:</strong> Item1|Item2|Item3 (com | entre os itens, sem espaços)</p>
</div>

<div class="form-group">
    {!! Form::label('itens_en', 'Itens *SEPARAR POR | (pipe ou barra vertical, sem espaços)* (INGLÊS)') !!}
    {!! Form::textarea('itens_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
    <p class="exemplo-separacao"><strong>Exemplo:</strong> Item1|Item2|Item3 (com | entre os itens, sem espaços)</p>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.naotecidos.aplicacoes-a-z.index') }}" class="btn btn-default btn-voltar">Voltar</a>