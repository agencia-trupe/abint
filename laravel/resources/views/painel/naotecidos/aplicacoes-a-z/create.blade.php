@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aplicações de A a Z /</small> Adicionar Aplicação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.naotecidos.aplicacoes-a-z.store', 'files' => true]) !!}

        @include('painel.naotecidos.aplicacoes-a-z.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
