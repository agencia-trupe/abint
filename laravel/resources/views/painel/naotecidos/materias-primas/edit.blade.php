@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Matérias-primas</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.materias-primas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.materias-primas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
