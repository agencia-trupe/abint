@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Identificação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.identificacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.identificacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
