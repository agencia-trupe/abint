@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Algumas Aplicações /</small> Editar Aplicação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.algumas-aplicacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.algumas-aplicacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
