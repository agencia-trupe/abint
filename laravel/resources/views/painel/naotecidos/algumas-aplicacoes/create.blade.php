@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Algumas Aplicações /</small> Adicionar Aplicação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.naotecidos.algumas-aplicacoes.store', 'files' => true]) !!}

        @include('painel.naotecidos.algumas-aplicacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
