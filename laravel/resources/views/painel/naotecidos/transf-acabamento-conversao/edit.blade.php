@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Transformação, acabamento e/ou conversão</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.transf-acabamento-conversao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.transf-acabamento-conversao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
