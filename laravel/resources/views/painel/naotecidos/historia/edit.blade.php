@extends('painel.common.template')

@section('content')

    <legend>
        <h2>História</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.historia.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.historia.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
