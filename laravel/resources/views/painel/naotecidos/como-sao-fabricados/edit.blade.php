@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Como são fabricados</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.como-sao-fabricados.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.como-sao-fabricados.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
