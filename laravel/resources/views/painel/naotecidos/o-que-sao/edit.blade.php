@extends('painel.common.template')

@section('content')

    <legend>
        <h2>O que são</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.naotecidos.o-que-sao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.naotecidos.o-que-sao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
