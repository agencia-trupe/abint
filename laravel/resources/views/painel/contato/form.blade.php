@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone_1', 'Telefone 1') !!}
    {!! Form::text('telefone_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone_2', 'Telefone 2') !!}
    {!! Form::text('telefone_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço Completo') !!}
    {!! Form::text('endereco', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco_en', 'Endereço Completo (INGLÊS)') !!}
    {!! Form::text('endereco_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('linkedin', 'LinkedIn') !!}
    {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
