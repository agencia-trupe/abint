@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home - Objetivos /</small> Editar Objetivo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.objetivos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.objetivos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
