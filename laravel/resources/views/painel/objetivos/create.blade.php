@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home - Objetivos /</small> Adicionar Objetivo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.objetivos.store', 'files' => true]) !!}

        @include('painel.objetivos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
