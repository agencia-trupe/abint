@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_motivos', 'Título Motivos') !!}
            {!! Form::text('titulo_motivos', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_motivos_en', 'Título Motivos (INGLÊS)') !!}
            {!! Form::text('titulo_motivos_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m1_titulo', 'Motivo 1 - Título') !!}
            {!! Form::text('m1_titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m1_titulo_en', 'Motivo 1 - Título (INGLÊS)') !!}
            {!! Form::text('m1_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m1_texto', 'Motivo 1 - Texto') !!}
            {!! Form::textarea('m1_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m1_texto_en', 'Motivo 1 - Texto (INGLÊS)') !!}
            {!! Form::textarea('m1_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m2_titulo', 'Motivo 2 - Título') !!}
            {!! Form::text('m2_titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m2_titulo_en', 'Motivo 2 - Título (INGLÊS)') !!}
            {!! Form::text('m2_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m2_texto', 'Motivo 2 - Texto') !!}
            {!! Form::textarea('m2_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m2_texto_en', 'Motivo 2 - Texto (INGLÊS)') !!}
            {!! Form::textarea('m2_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m3_titulo', 'Motivo 3 - Título') !!}
            {!! Form::text('m3_titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m3_titulo_en', 'Motivo 3 - Título (INGLÊS)') !!}
            {!! Form::text('m3_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m3_texto', 'Motivo 3 - Texto') !!}
            {!! Form::textarea('m3_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m3_texto_en', 'Motivo 3 - Texto (INGLÊS)') !!}
            {!! Form::textarea('m3_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m4_titulo', 'Motivo 4 - Título') !!}
            {!! Form::text('m4_titulo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m4_titulo_en', 'Motivo 4 - Título (INGLÊS)') !!}
            {!! Form::text('m4_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m4_texto', 'Motivo 4 - Texto') !!}
            {!! Form::textarea('m4_texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('m4_texto_en', 'Motivo 4 - Texto (INGLÊS)') !!}
            {!! Form::textarea('m4_texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('missao', 'Missão') !!}
            {!! Form::textarea('missao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('missao_en', 'Missão (INGLÊS)') !!}
            {!! Form::textarea('missao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('visao', 'Visão') !!}
            {!! Form::textarea('visao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('visao_en', 'Visão (INGLÊS)') !!}
            {!! Form::textarea('visao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('pilares', 'Pilares') !!}
            {!! Form::textarea('pilares', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('pilares_en', 'Pilares (INGLÊS)') !!}
            {!! Form::textarea('pilares_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textItens']) !!}
        </div>
    </div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}