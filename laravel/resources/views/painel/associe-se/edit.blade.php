@extends('painel.common.template')

@section('content')

    <legend>
        <h2>ASSOCIE-SE</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.associe-se.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.associe-se.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
