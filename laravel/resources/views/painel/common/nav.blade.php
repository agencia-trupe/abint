<ul class="nav navbar-nav">

  <li class="dropdown @if(Tools::routeIs(['painel.banners*', 'painel.objetivos*', 'painel.destaques*'])) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home<b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
      </li>
      <li @if(Tools::routeIs('painel.objetivos*')) class="active" @endif>
        <a href="{{ route('painel.objetivos.index') }}">Objetivos</a>
      </li>
      <li @if(Tools::routeIs('painel.destaques*')) class="active" @endif>
        <a href="{{ route('painel.destaques.index') }}">Destaques</a>
      </li>
    </ul>
  </li>

  <li class="dropdown @if(Tools::routeIs(['painel.institucional*', 'painel.diretoria*', 'painel.comites*'])) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">A ABINT<b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.institucional*')) class="active" @endif>
        <a href="{{ route('painel.institucional.index') }}">Institucional</a>
      </li>
      <li @if(Tools::routeIs('painel.diretoria*')) class="active" @endif>
        <a href="{{ route('painel.diretoria.index') }}">Diretoria</a>
      </li>
      <li @if(Tools::routeIs('painel.comites*')) class="active" @endif>
        <a href="{{ route('painel.comites.index') }}">Comitês Técnicos</a>
      </li>
    </ul>
  </li>

  <li @if(Tools::routeIs('painel.associados*')) class="active" @endif>
    <a href="{{ route('painel.associados.index') }}">Associados</a>
  </li>

  <li class="dropdown @if(Tools::routeIs(['painel.naotecidos.*'])) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">NãoTecidos<b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.naotecidos.o-que-sao*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.o-que-sao.index') }}">O que são</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.historia*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.historia.index') }}">História</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.materias-primas*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.materias-primas.index') }}">Matérias-primas</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.como-sao-fabricados*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.como-sao-fabricados.index') }}">Como são fabricados</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.transf-acabamento-conversao*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.transf-acabamento-conversao.index') }}">Transf., acabamento e/ou conversão</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.classificacao*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.classificacao.index') }}">Classificação</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.identificacao*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.identificacao.index') }}">Identificação</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.algumas-aplicacoes*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.algumas-aplicacoes.index') }}">Algumas aplicações</a>
      </li>
      <li @if(Tools::routeIs('painel.naotecidos.aplicacoes-a-z*')) class="active" @endif>
        <a href="{{ route('painel.naotecidos.aplicacoes-a-z.index') }}">Aplicações de A a Z</a>
      </li>
    </ul>
  </li>

  <li class="dropdown @if(Tools::routeIs(['painel.texteis-tecnicos.*'])) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Têxteis Técnicos<b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.texteis-tecnicos.o-que-sao*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.o-que-sao.index') }}">O que são</a>
      </li>
      <li @if(Tools::routeIs('painel.texteis-tecnicos.materias-primas*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.materias-primas.index') }}">Matérias-primas</a>
      </li>
      <li @if(Tools::routeIs('painel.texteis-tecnicos.como-sao-fabricados*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.como-sao-fabricados.index') }}">Como são fabricados</a>
      </li>
      <li @if(Tools::routeIs('painel.texteis-tecnicos.processos-transf-acabamento*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.processos-transf-acabamento.index') }}">Processos de transf. e acabamento</a>
      </li>
      <li @if(Tools::routeIs('painel.texteis-tecnicos.classificacao*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.classificacao.index') }}">Classificação</a>
      </li>
      <li @if(Tools::routeIs('painel.texteis-tecnicos.desempenho*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.desempenho.index') }}">Desempenho</a>
      </li>
      <li @if(Tools::routeIs('painel.texteis-tecnicos.identificacao*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.identificacao.index') }}">Identificação</a>
      </li>
      <li @if(Tools::routeIs('painel.texteis-tecnicos.aplicacoes*')) class="active" @endif>
        <a href="{{ route('painel.texteis-tecnicos.aplicacoes.index') }}">Aplicações</a>
      </li>
    </ul>
  </li>

  <li class="dropdown @if(Tools::routeIs(['painel.informacao*'])) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informação<b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.informacao.publicacoes*')) class="active" @endif>
        <a href="{{ route('painel.informacao.publicacoes.index') }}">Publicações</a>
      </li>
      <li @if(Tools::routeIs('painel.informacao.cursos-e-eventos*')) class="active" @endif>
        <a href="{{ route('painel.informacao.cursos-e-eventos.index') }}">Cursos e Eventos</a>
      </li>
      <li @if(Tools::routeIs('painel.informacao.normas-e-ensaios*')) class="active" @endif>
        <a href="{{ route('painel.informacao.normas-e-ensaios.index') }}">Normas e Ensaios</a>
      </li>
      <li @if(Tools::routeIs('painel.informacao.novidades*')) class="active" @endif>
        <a href="{{ route('painel.informacao.novidades.index') }}">Novidades/Imprensa</a>
      </li>
      <li @if(Tools::routeIs('painel.informacao.outros*')) class="active" @endif>
        <a href="{{ route('painel.informacao.outros.index') }}">Outros</a>
      </li>
    </ul>
  </li>

  <li class="dropdown @if(Tools::routeIs('painel.associe-se*')) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      Associe-se
      @if($contatosNaoLidosAssocieSe >= 1)
      <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidosAssocieSe }}</span>
      @endif
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.associe-se.index')) class="active" @endif>
        <a href="{{ route('painel.associe-se.index') }}">Associe-se</a>
      </li>
      <li @if(Tools::routeIs('painel.associe-se.contatos*')) class="active" @endif>
        <a href="{{ route('painel.associe-se.contatos.index') }}">
          Contatos Associe-se
          @if($contatosNaoLidosAssocieSe >= 1)
          <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidosAssocieSe }}</span>
          @endif
        </a>
      </li>
    </ul>
  </li>

  <li class="dropdown @if(Tools::routeIs(['painel.contato.index', 'painel.contato.recebidos*'])) active @endif">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      Contato
      @if($contatosNaoLidos >= 1)
      <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
      @endif
      <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
        <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
      </li>
      <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
        <a href="{{ route('painel.contato.recebidos.index') }}">
          Contatos Recebidos
          @if($contatosNaoLidos >= 1)
          <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
          @endif
        </a>
      </li>
    </ul>
  </li>
</ul>