@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home - Destaques /</small> Editar Destaque</h2>
    </legend>

    {!! Form::model($destaque, [
        'route'  => ['painel.destaques.update', $destaque->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.destaques.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
