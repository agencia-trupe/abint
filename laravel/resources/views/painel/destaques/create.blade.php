@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Home - Destaques /</small> Adicionar Destaque</h2>
    </legend>

    {!! Form::open(['route' => 'painel.destaques.store', 'files' => true]) !!}

        @include('painel.destaques.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
