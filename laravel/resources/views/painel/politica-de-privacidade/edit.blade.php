@extends('painel.common.template')

@section('content')

<legend>
    <h2>Política de Privacidade</h2>
</legend>

{!! Form::model($politica, [
'route' => ['painel.politica-de-privacidade.update', $politica->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.politica-de-privacidade.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection