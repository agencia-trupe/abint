@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<a href="{{ route('painel.comites.index') }}" title="Voltar para Comitês" class="btn btn-sm btn-default">&larr; Voltar para Comitês</a>

<legend>
    <h2>
        {{ $registro->nome }} | Membros
        <a href="{{ route('painel.comites.associados.create', $registro->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Membro</a>
    </h2>
</legend>

@if(!count($membros))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="comites_associados">
    <thead>
        <tr>
            <th>Ordem</th>
            <th>Imagem</th>
            <th>Nome</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($membros as $membro)
        <tr class="tr-row" id="{{ $membro->id }}">
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>
            <td><img src="{{ asset('assets/img/associados/'.$membro->associado_imagem) }}" style="width: 100%; max-width:100px;" alt=""></td>
            <td>{{ $membro->associado_nome }}</td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['painel.comites.associados.destroy', $registro->id, $membro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection