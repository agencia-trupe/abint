@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('associado_id', 'Associado') !!}
    {!! Form::select('associado_id', $associados , old('associado_id'), ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}