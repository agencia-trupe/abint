@extends('painel.common.template')

@section('content')

<legend>
    <h2><small>{{ $registro->nome }} /</small> Adicionar Membro</h2>
</legend>

{!! Form::model($associados, [
'route' => ['painel.comites.associados.store', $registro->id],
'method' => 'post',
'files' => true])
!!}

@include('painel.comites.associados.form', ['submitText' => 'Inserir'])

{!! Form::close() !!}

@endsection