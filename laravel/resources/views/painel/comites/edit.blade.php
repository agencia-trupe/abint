@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Comitês Técnicos /</small> Editar Comitê</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.comites.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.comites.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
