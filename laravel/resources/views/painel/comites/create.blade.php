@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Comitês Técnicos /</small> Adicionar Comitê</h2>
    </legend>

    {!! Form::open(['route' => 'painel.comites.store', 'files' => true]) !!}

        @include('painel.comites.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
