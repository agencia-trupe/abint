@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/associados/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="checkboxes">
    <span>Grupos:</span>
    @foreach($grupos as $grupo)
    <label style="display:block;">
        <input type="checkbox" name="grupo[]" value="{{ $grupo }}" @if(in_array($grupo, $associadoGrupos)) checked @endif>
        {{ $grupo }}
    </label>
    @endforeach
</div>
<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.associados.index') }}" class="btn btn-default btn-voltar">Voltar</a>