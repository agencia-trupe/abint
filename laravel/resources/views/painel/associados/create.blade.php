@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Associados /</small> Adicionar Associado</h2>
    </legend>

    {!! Form::open(['route' => 'painel.associados.store', 'files' => true]) !!}

        @include('painel.associados.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
