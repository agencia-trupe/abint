@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Associados /</small> Editar Associado</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.associados.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.associados.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
