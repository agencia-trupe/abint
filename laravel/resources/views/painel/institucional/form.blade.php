@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_en', 'Texto (INGLÊS)') !!}
    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('bienio_periodo', 'Biênio Período') !!}
    {!! Form::text('bienio_periodo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('bienio_periodo_en', 'Biênio Período (INGLÊS)') !!}
    {!! Form::text('bienio_periodo_en', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}