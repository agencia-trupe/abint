@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Normas e Ensaios /</small> Adicionar Norma/Ensaio</h2>
    </legend>

    {!! Form::open(['route' => 'painel.informacao.normas-e-ensaios.store', 'files' => true]) !!}

        @include('painel.informacao.normas-e-ensaios.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
