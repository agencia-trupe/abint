@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Normas e Ensaios /</small> Editar Norma/Ensaio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.informacao.normas-e-ensaios.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacao.normas-e-ensaios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
