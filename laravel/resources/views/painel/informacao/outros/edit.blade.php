@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Outros /</small> Editar Conteúdo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.informacao.outros.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacao.outros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
