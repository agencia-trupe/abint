@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Outros /</small> Adicionar Conteúdo</h2>
    </legend>

    {!! Form::open(['route' => 'painel.informacao.outros.store', 'files' => true]) !!}

        @include('painel.informacao.outros.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
