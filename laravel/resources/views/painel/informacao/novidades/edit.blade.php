@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Novidades/Imprensa /</small> Editar Novidade</h2>
    </legend>

    {!! Form::model($novidade, [
        'route'  => ['painel.informacao.novidades.update', $novidade->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacao.novidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
