@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Novidades/Imprensa /</small> Adicionar Novidade</h2>
    </legend>

    {!! Form::open(['route' => 'painel.informacao.novidades.store', 'files' => true]) !!}

        @include('painel.informacao.novidades.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
