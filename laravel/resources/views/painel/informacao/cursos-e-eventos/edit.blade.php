@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Cursos e Eventos /</small> Editar Curso/Evento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.informacao.cursos-e-eventos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacao.cursos-e-eventos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
