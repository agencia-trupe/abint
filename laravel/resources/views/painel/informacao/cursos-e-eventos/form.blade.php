@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('data', 'Data') !!}
    {!! Form::date('data', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    <img src="{{ url('assets/img/eventos/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome_en', 'Nome (INGLÊS)') !!}
    {!! Form::text('nome_en', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::label('periodo', 'Período do evento (opcional)') !!}
    {!! Form::text('periodo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('periodo_en', 'Período do evento (opcional) (INGLÊS)') !!}
    {!! Form::text('periodo_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao', 'Descrição') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('descricao_en', 'Descrição (INGLÊS)') !!}
    {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link do evento') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.informacao.cursos-e-eventos.index') }}" class="btn btn-default btn-voltar">Voltar</a>