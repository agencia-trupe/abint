@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Cursos e Eventos /</small> Adicionar Curso/Evento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.informacao.cursos-e-eventos.store', 'files' => true]) !!}

        @include('painel.informacao.cursos-e-eventos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
