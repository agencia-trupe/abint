@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Publicações /</small> Adicionar Publicação</h2>
    </legend>

    {!! Form::open(['route' => 'painel.informacao.publicacoes.store', 'files' => true]) !!}

        @include('painel.informacao.publicacoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
