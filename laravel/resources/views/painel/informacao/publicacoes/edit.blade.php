@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>INFORMAÇÃO - Publicações /</small> Editar Publicação</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.informacao.publicacoes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacao.publicacoes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
