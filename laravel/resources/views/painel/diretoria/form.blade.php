@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('cargo_id', 'Cargos') !!}
    {!! Form::select('cargo_id', $cargos , old('cargo_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('empresa', 'Empresa') !!}
    {!! Form::text('empresa', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.diretoria.index') }}" class="btn btn-default btn-voltar">Voltar</a>