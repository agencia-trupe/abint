@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Diretoria /</small> Editar Pessoa</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.diretoria.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.diretoria.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
