@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Diretoria /</small> Adicionar Pessoa</h2>
    </legend>

    {!! Form::open(['route' => 'painel.diretoria.store', 'files' => true]) !!}

        @include('painel.diretoria.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
