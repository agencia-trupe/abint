<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCargosTable extends Migration
{
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cargos');
    }
}
