<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateComitesTable extends Migration
{
    public function up()
    {
        Schema::create('comites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('nome');
            $table->string('nome_en');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('website')->nullable();
            $table->string('facebook');
            $table->string('linkedin');
            $table->string('instagram');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('comites');
    }
}
