<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContatoTable extends Migration
{
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone_1');
            $table->string('telefone_2');
            $table->string('endereco');
            $table->string('endereco_en');
            $table->string('instagram');
            $table->string('facebook');
            $table->string('linkedin');
            $table->text('google_maps');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contato');
    }
}
