<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNormasEnsaiosTable extends Migration
{
    public function up()
    {
        Schema::create('normas_ensaios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->text('descricao');
            $table->text('descricao_en');
            $table->string('imagem');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('normas_ensaios');
    }
}
