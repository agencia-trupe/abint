<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateDestaquesTable extends Migration
{
    public function up()
    {
        Schema::create('destaques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('destaques');
    }
}
