<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNaotecidosDefinicaoTable extends Migration
{
    public function up()
    {
        Schema::create('naotecidos_definicao', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('naotecidos_definicao');
    }
}
