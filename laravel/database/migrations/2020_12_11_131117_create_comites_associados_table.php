<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateComitesAssociadosTable extends Migration
{
    public function up()
    {
        Schema::create('comites_associados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('comite_id')->unsigned();
            $table->foreign('comite_id')->references('id')->on('comites')->onDelete('cascade');
            $table->integer('associado_id')->unsigned();
            $table->foreign('associado_id')->references('id')->on('associados')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('comites_associados');
    }
}
