<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNovidadesTable extends Migration
{
    public function up()
    {
        Schema::create('novidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->date('data');
            $table->string('slug');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->text('texto');
            $table->text('texto_en');
            $table->string('imagem');
            $table->string('arquivo')->nullable();
            $table->string('titulo_arquivo')->nullable();
            $table->string('titulo_arquivo_en')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('novidades');
    }
}
