<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateDiretoriaTable extends Migration
{
    public function up()
    {
        Schema::create('diretoria', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('cargo_id')->unsigned();
            $table->foreign('cargo_id')->references('id')->on('cargos')->onDelete('cascade');
            $table->string('nome');
            $table->string('empresa');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('diretoria');
    }
}
