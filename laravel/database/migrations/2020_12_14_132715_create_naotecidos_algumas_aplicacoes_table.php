<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNaotecidosAlgumasAplicacoesTable extends Migration
{
    public function up()
    {
        Schema::create('naotecidos_algumas_aplicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->text('texto');
            $table->text('texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('naotecidos_algumas_aplicacoes');
    }
}
