<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAssocieSeTable extends Migration
{
    public function up()
    {
        Schema::create('associe_se', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo_motivos');
            $table->string('titulo_motivos_en');
            $table->string('m1_titulo');
            $table->string('m1_titulo_en');
            $table->text('m1_texto');
            $table->text('m1_texto_en');
            $table->string('m2_titulo');
            $table->string('m2_titulo_en');
            $table->text('m2_texto');
            $table->text('m2_texto_en');
            $table->string('m3_titulo');
            $table->string('m3_titulo_en');
            $table->text('m3_texto');
            $table->text('m3_texto_en');
            $table->string('m4_titulo');
            $table->string('m4_titulo_en');
            $table->text('m4_texto');
            $table->text('m4_texto_en');
            $table->text('missao');
            $table->text('missao_en');
            $table->text('visao');
            $table->text('visao_en');
            $table->text('pilares');
            $table->text('pilares_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('associe_se');
    }
}
