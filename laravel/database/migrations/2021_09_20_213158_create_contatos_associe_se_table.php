<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContatosAssocieSeTable extends Migration
{
    public function up()
    {
        Schema::create('contatos_associe_se', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contatos_associe_se');
    }
}
