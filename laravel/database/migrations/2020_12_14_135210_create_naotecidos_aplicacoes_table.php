<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateNaotecidosAplicacoesTable extends Migration
{
    public function up()
    {
        Schema::create('naotecidos_aplicacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('titulo_en');
            $table->text('itens')->nullable();
            $table->text('itens_en')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('naotecidos_aplicacoes');
    }
}
