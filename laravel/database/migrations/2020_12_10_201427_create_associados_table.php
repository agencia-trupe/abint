<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAssociadosTable extends Migration
{
    public function up()
    {
        Schema::create('associados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('nome');
            $table->string('link');
            $table->string('grupo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('associados');
    }
}
