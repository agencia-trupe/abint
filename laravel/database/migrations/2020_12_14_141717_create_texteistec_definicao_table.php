<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTexteistecDefinicaoTable extends Migration
{
    public function up()
    {
        Schema::create('texteistec_definicao', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->text('texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('texteistec_definicao');
    }
}
