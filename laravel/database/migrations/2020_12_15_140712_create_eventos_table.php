<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEventosTable extends Migration
{
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->date('data');
            $table->string('nome');
            $table->string('nome_en');
            $table->string('periodo')->nullable();
            $table->string('periodo_en')->nullable();
            $table->text('descricao');
            $table->text('descricao_en');
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('eventos');
    }
}
