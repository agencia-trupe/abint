<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfiguracoesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configuracoes')->insert([
            'title'                      => 'ABINT',
            'description'                => 'Associação Brasileira das Indústrias de Nãotecidos e Tecidos Técnicos',
            'description_en'             => '',
            'keywords'                   => '',
            'imagem_de_compartilhamento' => '',
            'analytics_ua'               => '',
            'analytics_g'                => '',
            'codigo_gtm'                 => '',
            'tinify_key'                 => '',
        ]);
    }
}
