<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GruposTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('grupos')->insert([
            'id'        => 1,
            'ordem'     => 1,
            'titulo'    => 'Máquinas e Equipamentos',
            'titulo_en' => 'Machines and Equipment',
        ]);

        DB::table('grupos')->insert([
            'id'        => 2,
            'ordem'     => 2,
            'titulo'    => 'Matérias primas e Insumos',
            'titulo_en' => 'Raw materials and Inputs',
        ]);

        DB::table('grupos')->insert([
            'id'        => 3,
            'ordem'     => 3,
            'titulo'    => 'Fabricantes de Nãotecidos',
            'titulo_en' => 'Nonwovens Manufacturers',
        ]);

        DB::table('grupos')->insert([
            'id'        => 4,
            'ordem'     => 4,
            'titulo'    => 'Fabricantes de Tecidos e/ou Têxteis Técnicos',
            'titulo_en' => 'Technical Fabric and/or Textile Manufacturers',
        ]);

        DB::table('grupos')->insert([
            'id'        => 5,
            'ordem'     => 5,
            'titulo'    => 'Convertedores e Distribuidores',
            'titulo_en' => 'Converters and Distributors',
        ]);
    }
}
