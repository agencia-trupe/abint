<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InstitucionalTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'imagem'            => '',
            'texto'             => '',
            'texto_en'          => '',
            'bienio_periodo'    => 'Biênio Setembro/2020 a Setembro/2022',
            'bienio_periodo_en' => 'Biennium September/2020 to September/2022',
        ]);
    }
}
