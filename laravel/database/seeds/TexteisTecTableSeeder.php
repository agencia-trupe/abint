<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TexteisTecTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('texteistec_definicao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('texteistec_mat_primas')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('texteistec_fabricacao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('texteistec_processos')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('texteistec_classificacao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('texteistec_desempenho')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('texteistec_identificacao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('texteistec_aplicacoes')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);
    }
}
