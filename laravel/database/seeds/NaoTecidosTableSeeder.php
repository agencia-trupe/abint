<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NaoTecidosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('naotecidos_definicao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('naotecidos_historia')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('naotecidos_mat_primas')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('naotecidos_fabricacao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('naotecidos_transformacao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('naotecidos_classificacao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);

        DB::table('naotecidos_identificacao')->insert([
            'texto'    => '',
            'texto_en' => '',
        ]);
    }
}
