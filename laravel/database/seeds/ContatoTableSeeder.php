<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome'        => 'ABINT',
            'email'       => 'abint@abint.org.br',
            'telefone_1'  => '11 3032 3015',
            'telefone_2'  => '11 3823 6087',
            'endereco'    => 'Rua Marquês de Itu, 968 • Higienópolis • São Paulo/SP • 01223-000',
            'endereco_en' => '',
            'instagram'   => 'https://www.instagram.com/abintnaotecidos/',
            'facebook'    => 'https://www.facebook.com/ABINT-Assoc-Brasileira-das-Ind%C3%BAstrias-de-N%C3%A3otecidos-e-Tecidos-T%C3%A9cnicos-113884203789886',
            'linkedin'    => 'https://www.linkedin.com/company/abint/',
            'google_maps' => '',
        ]);
    }
}
