<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssocieSeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('associe_se')->insert([
            'titulo_motivos'    => 'Motivos para associar-se',
            'titulo_motivos_en' => 'Motivos para associar-se',
            'm1_titulo'         => 'APOIO COMERCIAL',
            'm1_titulo_en'      => 'APOIO COMERCIAL',
            'm1_texto'          => '',
            'm1_texto_en'       => '',
            'm2_titulo'         => 'APOIO TÉCNICO',
            'm2_titulo_en'      => 'APOIO TÉCNICO',
            'm2_texto'          => '',
            'm2_texto_en'       => '',
            'm3_titulo'         => 'ACESSO À INFORMAÇÃO',
            'm3_titulo_en'      => 'ACESSO À INFORMAÇÃO',
            'm3_texto'          => '',
            'm3_texto_en'       => '',
            'm4_titulo'         => 'PROMOÇÃO',
            'm4_titulo_en'      => 'PROMOÇÃO',
            'm4_texto'          => '',
            'm4_texto_en'       => '',
            'missao'            => '',
            'missao_en'         => '',
            'visao'             => '',
            'visao_en'          => '',
            'pilares'           => '',
            'pilares_en'        => '',
        ]);
    }
}
