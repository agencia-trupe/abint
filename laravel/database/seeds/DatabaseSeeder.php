<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(InstitucionalTableSeeder::class);
        $this->call(CargosTableSeeder::class);
        $this->call(GruposTableSeeder::class);
        $this->call(NaoTecidosTableSeeder::class);
        $this->call(TexteisTecTableSeeder::class);
        $this->call(PoliticaDePrivacidadeTableSeeder::class);
        $this->call(AssocieSeTableSeeder::class);
    }
}
