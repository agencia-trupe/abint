<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CargosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('cargos')->insert([
            'id'        => 1,
            'titulo'    => 'Presidente',
            'titulo_en' => 'President',
        ]);

        DB::table('cargos')->insert([
            'id'        => 2,
            'titulo'    => 'Vice-Presidente',
            'titulo_en' => 'Vice President',
        ]);

        DB::table('cargos')->insert([
            'id'        => 3,
            'titulo'    => 'Diretor',
            'titulo_en' => 'Director',
        ]);

        DB::table('cargos')->insert([
            'id'        => 4,
            'titulo'    => 'Conselheiro',
            'titulo_en' => 'Counselor',
        ]);
    }
}
