<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TexteisTecProcessos extends Model
{
    protected $table = 'texteistec_processos';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%');
    }
}
