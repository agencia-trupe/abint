<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NaoTecidosHistoria extends Model
{
    protected $table = 'naotecidos_historia';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%');
    }
}
