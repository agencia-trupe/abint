<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Objetivo extends Model
{
    protected $table = 'objetivos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%'.$termo.'%')
                     ->orWhere('titulo_en', 'LIKE', '%'.$termo.'%');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 200,
            'height' => 200,
            'path'   => 'assets/img/objetivos/'
        ]);
    }
}
