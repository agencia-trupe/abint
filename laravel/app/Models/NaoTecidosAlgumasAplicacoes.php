<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class NaoTecidosAlgumasAplicacoes extends Model
{
    protected $table = 'naotecidos_algumas_aplicacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%'.$termo.'%')
                     ->orWhere('titulo_en', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'   => 150,
                'height'  => 150,
                'path'    => 'assets/img/naotecidos-aplicacoes/thumbs/'
            ],
            [
                'width'  => 1200,
                'height' => null,
                'upsize'  => true,
                'path'    => 'assets/img/naotecidos-aplicacoes/'
            ]
        ]);
    }
}
