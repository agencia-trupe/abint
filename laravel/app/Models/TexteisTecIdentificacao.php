<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TexteisTecIdentificacao extends Model
{
    protected $table = 'texteistec_identificacao';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%');
    }
}
