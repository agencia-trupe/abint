<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Associado extends Model
{
    protected $table = 'associados';

    protected $guarded = ['id'];

    protected $casts = [
        'grupos' => 'array'
    ];

    public function scopeBusca($query, $termo)
    {
        return $query->where('nome', 'LIKE', '%'.$termo.'%');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 300,
            'height' => null,
            'path'   => 'assets/img/associados/'
        ]);
    }
}
