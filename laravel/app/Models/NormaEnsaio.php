<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class NormaEnsaio extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'normas_ensaios';

    protected $guarded = ['id'];

    protected $fillable = [
        'titulo', 'titulo_en', 'descricao', 'descricao_en', 'imagem', 'arquivo'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('titulo_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('descricao', 'LIKE', '%' . $termo . '%')
            ->orWhere('descricao_en', 'LIKE', '%' . $termo . '%');
    }

    public static function upload_imagem()
    {
        return CropImageTinify::make('imagem', [
            'width'  => 200,
            'height' => 270,
            'path'   => 'assets/img/normas-e-ensaios/'
        ]);
    }
}
