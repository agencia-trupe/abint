<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Diretoria extends Model
{
    protected $table = 'diretoria';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('nome', 'LIKE', '%'.$termo.'%')
                     ->orWhere('empresa', 'LIKE', '%'.$termo.'%');
    }
}
