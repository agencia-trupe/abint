<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Institucional extends Model
{
    protected $table = 'institucional';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%')
                     ->orWhere('bienio_periodo', 'LIKE', '%'.$termo.'%')
                     ->orWhere('bienio_periodo_en', 'LIKE', '%'.$termo.'%');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 800,
            'height' => null,
            'path'   => 'assets/img/institucional/'
        ]);
    }
}
