<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NaoTecidosAplicacoes extends Model
{
    protected $table = 'naotecidos_aplicacoes';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%'.$termo.'%')
                     ->orWhere('titulo_en', 'LIKE', '%'.$termo.'%')
                     ->orWhere('itens', 'LIKE', '%'.$termo.'%')
                     ->orWhere('itens_en', 'LIKE', '%'.$termo.'%');
    }
}
