<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComiteAssociado extends Model
{
    protected $table = 'comites_associados';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeComite($query, $id)
    {
        return $query->where('comite_id', $id);
    }
}
