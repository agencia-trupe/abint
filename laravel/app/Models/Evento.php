<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    protected $table = 'eventos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        if (!empty(Configuracoes::first()->tinify_key)) {
            return CropImageTinify::make('imagem', [
                [
                    'width'  => 280,
                    'height' => null,
                    'path'   => 'assets/img/eventos/'
                ],
                [
                    'width'  => null,
                    'height' => null,
                    'path'   => 'assets/img/eventos/originais/'
                ]
            ]);
        } else {
            return CropImage::make('imagem', [
                [
                    'width'  => 280,
                    'height' => null,
                    'path'   => 'assets/img/eventos/'
                ],
                [
                    'width'  => null,
                    'height' => null,
                    'path'   => 'assets/img/eventos/originais/'
                ]
            ]);
        }
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('nome', 'LIKE', '%' . $termo . '%')
            ->orWhere('nome_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('descricao', 'LIKE', '%' . $termo . '%')
            ->orWhere('descricao_en', 'LIKE', '%' . $termo . '%');
    }
}
