<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Model;

class Comite extends Model
{
    protected $table = 'comites';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function associados()
    {
        return $this->hasMany('App\Models\ComiteAssociado', 'comite_id')->ordenados();
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('nome', 'LIKE', '%'.$termo.'%')
                     ->orWhere('nome_en', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 400,
            'height' => null,
            'path'   => 'assets/img/comites/'
        ]);
    }
}
