<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = 'grupos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function associados()
    {
        return $this->hasMany('App\Models\Associado', 'associado_id')->ordenados();
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%'.$termo.'%')
                     ->orWhere('titulo_en', 'LIKE', '%'.$termo.'%');
    }
}
