<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssocieSe extends Model
{
    protected $table = 'associe_se';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('m1_titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('m1_titulo_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('m1_texto', 'LIKE', '%' . $termo . '%')
            ->orWhere('m1_texto_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('m2_titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('m2_titulo_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('m2_texto', 'LIKE', '%' . $termo . '%')
            ->orWhere('m2_texto_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('m3_titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('m3_titulo_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('m3_texto', 'LIKE', '%' . $termo . '%')
            ->orWhere('m3_texto_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('m4_titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('m4_titulo_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('m4_texto', 'LIKE', '%' . $termo . '%')
            ->orWhere('m4_texto_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('missao', 'LIKE', '%' . $termo . '%')
            ->orWhere('missao_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('visao', 'LIKE', '%' . $termo . '%')
            ->orWhere('visao_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('pilares', 'LIKE', '%' . $termo . '%')
            ->orWhere('pilares_en', 'LIKE', '%' . $termo . '%');
    }
}
