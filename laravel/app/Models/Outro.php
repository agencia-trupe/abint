<?php

namespace App\Models;

use App\Helpers\CropImage;
use App\Helpers\CropImageTinify;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Outro extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'outros';

    protected $guarded = ['id'];

    protected $fillable = [
        'data', 'titulo', 'titulo_en', 'texto', 'texto_en', 'imagem', 'arquivo', 'titulo_arquivo', 'titulo_arquivo_en'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        if (!empty(Configuracoes::first()->tinify_key)) {
            return CropImageTinify::make('imagem', [
                [
                    'width'  => 300,
                    'height' => null,
                    'path'   => 'assets/img/outros/lista/'
                ],
                [
                    'width'  => 800,
                    'height' => null,
                    'path'   => 'assets/img/outros/'
                ]
            ]);
        } else {
            return CropImage::make('imagem', [
                [
                    'width'  => 300,
                    'height' => null,
                    'path'   => 'assets/img/outros/lista/'
                ],
                [
                    'width'  => 800,
                    'height' => null,
                    'path'   => 'assets/img/outros/'
                ]
            ]);
        }
    }

    public function scopeBusca($query, $termo)
    {
        return $query->where('titulo', 'LIKE', '%' . $termo . '%')
            ->orWhere('titulo_en', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto', 'LIKE', '%' . $termo . '%')
            ->orWhere('texto_en', 'LIKE', '%' . $termo . '%');
    }
}
