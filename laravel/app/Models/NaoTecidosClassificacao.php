<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NaoTecidosClassificacao extends Model
{
    protected $table = 'naotecidos_classificacao';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%');
    }
}
