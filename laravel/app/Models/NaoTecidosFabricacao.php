<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NaoTecidosFabricacao extends Model
{
    protected $table = 'naotecidos_fabricacao';

    protected $guarded = ['id'];

    public function scopeBusca($query, $termo)
    {
        return $query->where('texto', 'LIKE', '%'.$termo.'%')
                     ->orWhere('texto_en', 'LIKE', '%'.$termo.'%');
    }
}
