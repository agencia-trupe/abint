<?php

namespace App\Providers;

use App\Models\AceiteDeCookies;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer('*', function ($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('frontend.*', function ($view) {
            $request = app(\Illuminate\Http\Request::class);
            $view->with('verificacao', AceiteDeCookies::where('ip', $request->ip())->first());

            if (session()->get('locale') == "pt" || session()->get('locale') == null) {
                setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            } else {
                setlocale(LC_TIME, 'en', 'en-US');
            }
        });

        view()->composer('frontend.common.*', function ($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('grupos', \App\Models\Grupo::ordenados()->get());
        });

        view()->composer('painel.common.*', function ($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('contatosNaoLidosAssocieSe', \App\Models\ContatoAssocieSe::naoLidos()->count());
        });
    }

    public function register()
    {
        //
    }
}
