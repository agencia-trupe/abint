<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'App\Http\Controllers';

    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');
        $router->model('objetivos', 'App\Models\Objetivo');
        $router->model('destaques', 'App\Models\Destaque');
        $router->model('institucional', 'App\Models\Institucional');
        $router->model('cargos', 'App\Models\Cargo');
        $router->model('diretoria', 'App\Models\Diretoria');
        $router->model('grupos', 'App\Models\Grupo');
        $router->model('associados', 'App\Models\Associado');
        $router->model('comites', 'App\Models\Comite');
        $router->model('comites_associados', 'App\Models\ComiteAssociado');
        $router->model('naotecidos_definicao', 'App\Models\NaoTecidosDefinicao');
        $router->model('naotecidos_historia', 'App\Models\NaoTecidosHistoria');
        $router->model('naotecidos_mat_primas', 'App\Models\NaoTecidosMatPrimas');
        $router->model('naotecidos_fabricacao', 'App\Models\NaoTecidosFabricacao');
        $router->model('naotecidos_transformacao', 'App\Models\NaoTecidosTransformacao');
        $router->model('naotecidos_classificacao', 'App\Models\NaoTecidosClassificacao');
        $router->model('naotecidos_identificacao', 'App\Models\NaoTecidosIdentificacao');
        $router->model('naotecidos_algumas_aplicacoes', 'App\Models\NaoTecidosAlgumasAplicacoes');
        $router->model('naotecidos_aplicacoes', 'App\Models\NaoTecidosAplicacoes');
        $router->model('texteistec_definicao', 'App\Models\TexteisTecDefinicao');
        $router->model('texteistec_mat_primas', 'App\Models\TexteisTecMatPrimas');
        $router->model('texteistec_fabricacao', 'App\Models\TexteisTecFabricacao');
        $router->model('texteistec_processos', 'App\Models\TexteisTecProcessos');
        $router->model('texteistec_classificacao', 'App\Models\TexteisTecClassificacao');
        $router->model('texteistec_desempenho', 'App\Models\TexteisTecDesempenho');
        $router->model('texteistec_identificacao', 'App\Models\TexteisTecIdentificacao');
        $router->model('texteistec_aplicacoes', 'App\Models\TexteisTecAplicacoes');
        $router->model('publicacoes', 'App\Models\Publicacao');
        $router->model('eventos', 'App\Models\Evento');
        $router->model('normas_ensaios', 'App\Models\NormaEnsaio');
        $router->model('outros', 'App\Models\Outro');
        $router->model('novidades', 'App\Models\Novidade');
        $router->model('associe_se', 'App\Models\AssocieSe');
        $router->model('contatos_associe_se', 'App\Models\ContatoAssocieSe');
        $router->model('aceite_de_cookies', 'App\Models\AceiteDeCookies');
        $router->model('politica_de_privacidade', 'App\Models\PoliticaDePrivacidade');
        $router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
