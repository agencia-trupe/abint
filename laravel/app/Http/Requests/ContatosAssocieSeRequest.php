<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosAssocieSeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => '',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
            'email'    => trans('frontend.request-email'),
        ];
    }
}
