<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ObjetivosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'    => 'required|image',
            'titulo'    => 'required',
            'titulo_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
            'image'    => trans('frontend.request-image'),
        ];
    }
}
