<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EventosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem'       => 'required|image',
            'nome'         => 'required',
            'nome_en'      => '',
            'data'         => 'required',
            'periodo'      => '',
            'periodo_en'   => '',
            'descricao'    => 'required',
            'descricao_en' => '',
            'link'         => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
            'image'    => trans('frontend.request-image'),
        ];
    }
}
