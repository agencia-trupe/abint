<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NaoTecidosGeralRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto'    => 'required',
            'texto_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
        ];
    }
}
