<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TexteisTecGeralRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto'    => 'required',
            'texto_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
        ];
    }
}
