<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'              => 'required',
            'email'             => 'required|email',
            'telefone_1'        => 'required',
            'telefone_2'        => 'required',
            'endereco'          => 'required',
            'endereco_en'       => 'required',
            'facebook'          => 'required',
            'instagram'         => 'required',
            'linkedin'          => 'required',
            'google_maps'       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
            'email'    => trans('frontend.request-email'),
        ];
    }
}
