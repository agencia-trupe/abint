<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DiretoriaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'    => 'required',
            'empresa' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
            'image'    => trans('frontend.request-image'),
        ];
    }
}
