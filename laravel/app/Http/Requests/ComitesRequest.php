<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ComitesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome'      => 'required',
            'nome_en'   => '',
            'texto'     => 'required',
            'texto_en'  => '',
            'imagem'    => 'image',
            'facebook'  => 'required',
            'linkedin'  => 'required',
            'instagram' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
            'image'    => trans('frontend.request-image'),
        ];
    }
}
