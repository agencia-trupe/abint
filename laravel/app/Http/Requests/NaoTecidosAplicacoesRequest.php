<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NaoTecidosAplicacoesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo'    => 'required',
            'titulo_en' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
        ];
    }
}
