<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AssocieSeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'titulo_motivos'    => 'required',
            'titulo_motivos_en' => '',
            'm1_titulo'         => 'required',
            'm1_titulo_en'      => '',
            'm1_texto'          => 'required',
            'm1_texto_en'       => '',
            'm2_titulo'         => 'required',
            'm2_titulo_en'      => '',
            'm2_texto'          => 'required',
            'm2_texto_en'       => '',
            'm3_titulo'         => 'required',
            'm3_titulo_en'      => '',
            'm3_texto'          => 'required',
            'm3_texto_en'       => '',
            'm4_titulo'         => 'required',
            'm4_titulo_en'      => '',
            'm4_texto'          => 'required',
            'm4_texto_en'       => '',
            'missao'            => 'required',
            'missao_en'         => '',
            'visao'             => 'required',
            'visao_en'          => '',
            'pilares'           => 'required',
            'pilares_en'        => '',
        ];
    }

    public function messages()
    {
        return [
            'required' => trans('frontend.request-required'),
        ];
    }
}
