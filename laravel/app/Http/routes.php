<?php

use App\Helpers\Tools;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('a-abint', 'AAbintController@index')->name('a-abint');
    Route::get('associados', 'AssociadosController@index')->name('associados');

    Route::get('naotecidos/{submenu}', 'NaoTecidosController@index')->name('naotecidos.index');
    Route::get('naotecidos/list/{submenu}', 'NaoTecidosController@getDados')->name('naotecidos.getDados');
    Route::get('naotecidos/aplicacoes-a-z/{letra}', 'NaoTecidosController@getAplicacaoLetra')->name('naotecidos.getAplicacaoLetra');

    Route::get('texteis-tecnicos/{submenu}', 'TexteisTecnicosController@index')->name('texteis-tecnicos.index');
    Route::get('texteis-tecnicos/list/{submenu}', 'TexteisTecnicosController@getDadosTexteis')->name('texteis-tecnicos.getDadosTexteis');

    Route::get('informacao/{submenu}', 'InformacaoController@index')->name('informacao.index');
    Route::get('informacao/list/{submenu}', 'InformacaoController@getDadosInformacao')->name('informacao.getDadosInformacao');
    Route::get('informacao/publicacoes/download/{slug}', 'InformacaoController@downloadPublicacao')->name('publicacoes.download');
    Route::get('informacao/normas-e-ensaios/download/{slug}', 'InformacaoController@downloadNormaEnsaio')->name('normas-e-ensaios.download');
    Route::get('informacao/imprensa/{novidade}', 'InformacaoController@showNovidade')->name('novidades.show');
    Route::get('informacao/imprensa/download/{novidade}', 'InformacaoController@downloadNovidade')->name('novidades.download');
    Route::get('informacao/outros/{outro}', 'InformacaoController@showOutro')->name('outros.show');
    Route::get('informacao/outros/download/{outro}', 'InformacaoController@downloadOutro')->name('outros.download');

    Route::get('associe-se', 'AssocieSeController@index')->name('associe-se');
    Route::post('associe-se', 'AssocieSeController@post')->name('associe-se.post');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('busca', 'BuscaController@post')->name('busca.post');
    Route::get('politica-de-privacidade', 'PoliticaDePrivacidadeController@index')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');

    // Idiomas
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt'])) {
            Session::put('locale', $idioma);
        }
        if (URL::previous() == route('busca.post')) {
            return redirect()->route('home');
        } else {
            return redirect()->back();
        }
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', 'PainelController@index')->name('painel');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
        Route::resource('banners', 'BannersController');
        Route::resource('objetivos', 'ObjetivosController');
        Route::resource('destaques', 'DestaquesController');
        Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);
        Route::resource('diretoria', 'DiretoriaController');
        Route::resource('associados', 'AssociadosController');
        Route::resource('comites', 'ComitesController', ['except' => 'show']);
        Route::resource('comites.associados', 'ComitesAssociadosController', ['only' => ['index', 'create', 'store', 'destroy']]);
        Route::group([
            'prefix'     => 'naotecidos',
            'namespace'  => 'NaoTecidos',
        ], function () {
            Route::resource('o-que-sao', 'NaoTecidosDefinicaoController', ['only' => ['index', 'update']]);
            Route::resource('historia', 'NaoTecidosHistoriaController', ['only' => ['index', 'update']]);
            Route::resource('materias-primas', 'NaoTecidosMatPrimasController', ['only' => ['index', 'update']]);
            Route::resource('como-sao-fabricados', 'NaoTecidosFabricacaoController', ['only' => ['index', 'update']]);
            Route::resource('transf-acabamento-conversao', 'NaoTecidosTransformacaoController', ['only' => ['index', 'update']]);
            Route::resource('classificacao', 'NaoTecidosClassificacaoController', ['only' => ['index', 'update']]);
            Route::resource('identificacao', 'NaoTecidosIdentificacaoController', ['only' => ['index', 'update']]);
            Route::resource('algumas-aplicacoes', 'NaoTecidosAlgumasAplicacoesController');
            Route::resource('aplicacoes-a-z', 'NaoTecidosAplicacoesController');
        });
        Route::group([
            'prefix'     => 'texteis-tecnicos',
            'namespace'  => 'TexteisTecnicos',
        ], function () {
            Route::resource('o-que-sao', 'TexteisTecDefinicaoController', ['only' => ['index', 'update']]);
            Route::resource('materias-primas', 'TexteisTecMatPrimasController', ['only' => ['index', 'update']]);
            Route::resource('como-sao-fabricados', 'TexteisTecFabricacaoController', ['only' => ['index', 'update']]);
            Route::resource('processos-transf-acabamento', 'TexteisTecProcessosController', ['only' => ['index', 'update']]);
            Route::resource('classificacao', 'TexteisTecClassificacaoController', ['only' => ['index', 'update']]);
            Route::resource('desempenho', 'TexteisTecDesempenhoController', ['only' => ['index', 'update']]);
            Route::resource('identificacao', 'TexteisTecIdentificacaoController', ['only' => ['index', 'update']]);
            Route::resource('aplicacoes', 'TexteisTecAplicacoesController', ['only' => ['index', 'update']]);
        });
        Route::group([
            'prefix'     => 'informacao',
            'namespace'  => 'Informacao',
        ], function () {
            Route::resource('publicacoes', 'PublicacoesController');
            Route::resource('cursos-e-eventos', 'EventosController');
            Route::resource('normas-e-ensaios', 'NormasEnsaiosController');
            Route::resource('novidades', 'NovidadesController'); // IMPRENSA
            Route::resource('outros', 'OutrosController');
        });
        Route::resource('associe-se', 'AssocieSeController', ['only' => ['index', 'update']]);
        Route::get('associe-se/contatos/{associe}/toggle', ['as' => 'painel.associe-se.contatos.toggle', 'uses' => 'ContatosAssocieSeController@toggle']);
        Route::resource('associe-se/contatos', 'ContatosAssocieSeController');
        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');
        Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
        Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
        Route::resource('usuarios', 'UsuariosController');

        // Limpar caches
        Route::get('clear-cache', function () {
            $exitCode = Artisan::call('config:clear');
            $exitCode = Artisan::call('cache:clear');
            $exitCode = Artisan::call('route:clear');
            $exitCode = Artisan::call('view:clear');
            $exitCode = Artisan::call('config:cache');

            return 'DONE';
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function () {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
        Route::get('esqueci-minha-senha', 'PasswordController@sendEmail')->name('esqueci-minha-senha');
        Route::post('esqueci-minha-senha', 'PasswordController@sendResetLinkEmail')->name('esqueci-minha-senha.post');
        Route::get('reset/{token}', 'PasswordController@showResetForm')->name('password-reset');
        Route::post('reset', 'PasswordController@reset')->name('password-reset.post');
    });
});
