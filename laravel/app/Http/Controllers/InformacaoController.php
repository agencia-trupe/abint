<?php

namespace App\Http\Controllers;

use App\Models\Evento;
use App\Models\NormaEnsaio;
use App\Models\Novidade;
use App\Models\Outro;
use App\Models\Publicacao;
use Illuminate\Support\Facades\Lang;

class InformacaoController extends Controller
{
    public function index($submenu)
    {
        $publicacoes = null;
        $cursosEventos = null;
        $normasEnsaios = null;
        $novidades = null; // IMPRENSA
        $outros = null;

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        // $relatorios = Relatorio::where('tipo', 'adição')->where('created_at', 'LIKE', "%$dia1%")->get();

        // $data = Novidade::where('data', '2022-02-01')->get();

        // $novidades2 = Novidade::ordenados()->get();

        // $datanow = strftime('%d %B %Y', strtotime($novidades2[2]->data));

        // dd($datanow);

        switch ($submenu) {
            case "publicacoes":
                $titulo = Lang::get('frontend.informacao.publicacoes');
                $publicacoes = Publicacao::ordenados()->get();
                break;
            case "cursos-e-eventos":
                $titulo = Lang::get('frontend.informacao.cursos-e-eventos');
                $cursosEventos = Evento::ordenados()->get();
                break;
            case "normas-e-ensaios":
                $titulo = Lang::get('frontend.informacao.normas-e-ensaios');
                $normasEnsaios = NormaEnsaio::ordenados()->get();
                break;
            case "imprensa":
                $titulo = Lang::get('frontend.informacao.imprensa');
                $novidades = Novidade::ordenados()->get();
                break;
            case "outros":
                $titulo = Lang::get('frontend.informacao.outros');
                $outros = Outro::ordenados()->get();
                break;
        }

        return view('frontend.informacao', compact('titulo', 'publicacoes', 'cursosEventos', 'normasEnsaios', 'novidades', 'outros'));
    }

    public function getDadosInformacao($submenu)
    {
        switch ($submenu) {
            case "publicacoes":
                $titulo = Lang::get('frontend.informacao.publicacoes');
                $resultado = Publicacao::ordenados()->get();
                break;
            case "cursos-e-eventos":
                $titulo = Lang::get('frontend.informacao.cursos-e-eventos');
                $resultado = Evento::ordenados()->get();
                break;
            case "normas-e-ensaios":
                $titulo = Lang::get('frontend.informacao.normas-e-ensaios');
                $resultado = NormaEnsaio::ordenados()->get();
                break;
            case "imprensa":
                $titulo = Lang::get('frontend.informacao.imprensa');
                $resultado = Novidade::ordenados()->get();
                break;
            case "outros":
                $titulo = Lang::get('frontend.informacao.outros');
                $resultado = Outro::ordenados()->get();
                break;
        }

        return response()->json(['submenu' => $submenu, 'resultado' => $resultado, 'titulo' => $titulo]);
    }

    public function showNovidade($novidade_slug)
    {
        $novidade = Novidade::where('slug', $novidade_slug)->first();

        return view('frontend.novidades-show', compact('novidade'));
    }

    public function showOutro($outro_slug)
    {
        $outro = Outro::where('slug', $outro_slug)->first();

        return view('frontend.outros-show', compact('outro'));
    }

    public function downloadPublicacao($slug)
    {
        $arquivo = Publicacao::where('slug', $slug)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/pdf/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }

    public function downloadNormaEnsaio($slug)
    {
        $arquivo = NormaEnsaio::where('slug', $slug)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/pdf/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }

    public function downloadNovidade($slug)
    {
        $arquivo = Novidade::where('slug', $slug)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/pdf/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }

    public function downloadOutro($slug)
    {
        $arquivo = Outro::where('slug', $slug)->select('arquivo')->first();

        $arquivoPath = public_path() . "/assets/pdf/" . $arquivo->arquivo;

        return response()->file($arquivoPath);
    }
}
