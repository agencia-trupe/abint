<?php

namespace App\Http\Controllers;

use App\Models\TexteisTecAplicacoes;
use App\Models\TexteisTecClassificacao;
use App\Models\TexteisTecDefinicao;
use App\Models\TexteisTecDesempenho;
use App\Models\TexteisTecFabricacao;
use App\Models\TexteisTecIdentificacao;
use App\Models\TexteisTecMatPrimas;
use App\Models\TexteisTecProcessos;
use Illuminate\Support\Facades\Lang;

class TexteisTecnicosController extends Controller
{
    public function index($submenu)
    {
        switch ($submenu) {
            case "o-que-sao":
                $titulo = Lang::get('frontend.texteis-tec.titulo-o-que-sao');
                $resultado = TexteisTecDefinicao::first();
                break;
            case "materias-primas":
                $titulo = Lang::get('frontend.texteis-tec.titulo-mat-primas');
                $resultado = TexteisTecMatPrimas::first();
                break;
            case "como-sao-fabricados":
                $titulo = Lang::get('frontend.texteis-tec.titulo-fabricacao');
                $resultado = TexteisTecFabricacao::first();
                break;
            case "processo-transf-acabamento":
                $titulo = Lang::get('frontend.texteis-tec.titulo-processos');
                $resultado = TexteisTecProcessos::first();
                break;
            case "classificacao":
                $titulo = Lang::get('frontend.texteis-tec.titulo-classificacao');
                $resultado = TexteisTecClassificacao::first();
                break;
            case "desempenho":
                $titulo = Lang::get('frontend.texteis-tec.titulo-desempenho');
                $resultado = TexteisTecDesempenho::first();
                break;
            case "identificacao":
                $titulo = Lang::get('frontend.texteis-tec.titulo-identificacao');
                $resultado = TexteisTecIdentificacao::first();
                break;
            case "algumas-aplicacoes":
                $titulo = Lang::get('frontend.texteis-tec.titulo-aplicacoes');
                $resultado = TexteisTecAplicacoes::first();
                break;
        }

        return view('frontend.texteis-tecnicos', compact('titulo', 'resultado'));
    }

    public function getDadosTexteis($submenu)
    {
        switch ($submenu) {
            case "o-que-sao":
                $titulo = Lang::get('frontend.texteis-tec.titulo-o-que-sao');
                $resultado = TexteisTecDefinicao::first();
                break;
            case "materias-primas":
                $titulo = Lang::get('frontend.texteis-tec.titulo-mat-primas');
                $resultado = TexteisTecMatPrimas::first();
                break;
            case "como-sao-fabricados":
                $titulo = Lang::get('frontend.texteis-tec.titulo-fabricacao');
                $resultado = TexteisTecFabricacao::first();
                break;
            case "processo-transf-acabamento":
                $titulo = Lang::get('frontend.texteis-tec.titulo-processos');
                $resultado = TexteisTecProcessos::first();
                break;
            case "classificacao":
                $titulo = Lang::get('frontend.texteis-tec.titulo-classificacao');
                $resultado = TexteisTecClassificacao::first();
                break;
            case "desempenho":
                $titulo = Lang::get('frontend.texteis-tec.titulo-desempenho');
                $resultado = TexteisTecDesempenho::first();
                break;
            case "identificacao":
                $titulo = Lang::get('frontend.texteis-tec.titulo-identificacao');
                $resultado = TexteisTecIdentificacao::first();
                break;
            case "algumas-aplicacoes":
                $titulo = Lang::get('frontend.texteis-tec.titulo-aplicacoes');
                $resultado = TexteisTecAplicacoes::first();
                break;
        }

        return response()->json(['submenu' => $submenu, 'resultado' => $resultado, 'titulo' => $titulo]);
    }
}
