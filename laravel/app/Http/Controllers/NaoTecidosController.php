<?php

namespace App\Http\Controllers;

use App\Models\NaoTecidosAlgumasAplicacoes;
use App\Models\NaoTecidosAplicacoes;
use App\Models\NaoTecidosClassificacao;
use App\Models\NaoTecidosDefinicao;
use App\Models\NaoTecidosFabricacao;
use App\Models\NaoTecidosHistoria;
use App\Models\NaoTecidosIdentificacao;
use App\Models\NaoTecidosMatPrimas;
use App\Models\NaoTecidosTransformacao;
use Illuminate\Support\Facades\Lang;

class NaoTecidosController extends Controller
{
    public function index($submenu)
    {
        $algumasAplicacoes = null;
        $aplicacoesAZ = null;

        switch ($submenu) {
            case "o-que-sao":
                $titulo = Lang::get('frontend.naotecidos.titulo-o-que-sao');
                $resultado = NaoTecidosDefinicao::first();
                break;
            case "historia":
                $titulo = Lang::get('frontend.naotecidos.titulo-historia');
                $resultado = NaoTecidosHistoria::first();
                break;
            case "materias-primas":
                $titulo = Lang::get('frontend.naotecidos.titulo-mat-primas');
                $resultado = NaoTecidosMatPrimas::first();
                break;
            case "como-sao-fabricados":
                $titulo = Lang::get('frontend.naotecidos.titulo-fabricacao');
                $resultado = NaoTecidosFabricacao::first();
                break;
            case "transf-acabamento-conversao":
                $titulo = Lang::get('frontend.naotecidos.titulo-transformacao');
                $resultado = NaoTecidosTransformacao::first();
                break;
            case "classificacao":
                $titulo = Lang::get('frontend.naotecidos.titulo-classificacao');
                $resultado = NaoTecidosClassificacao::first();
                break;
            case "identificacao":
                $titulo = Lang::get('frontend.naotecidos.titulo-identificacao');
                $resultado = NaoTecidosIdentificacao::first();
                break;
            case "algumas-aplicacoes":
                $titulo = Lang::get('frontend.naotecidos.titulo-aplicacoes');
                $algumasAplicacoes = NaoTecidosAlgumasAplicacoes::ordenados()->get();
                break;
            case "aplicacoes-a-z":
                $titulo = Lang::get('frontend.naotecidos.titulo-aplicacoes-a-z');
                $aplicacoesAZ = NaoTecidosAplicacoes::orderBy('titulo', 'ASC')->get();
                break;
        }

        $letras = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","W","Y","Z"];

        return view('frontend.naotecidos', compact('titulo', 'resultado', 'algumasAplicacoes', 'aplicacoesAZ', 'letras'));
    }

    public function getDados($submenu)
    {
        switch ($submenu) {
            case "o-que-sao":
                $titulo = Lang::get('frontend.naotecidos.titulo-o-que-sao');
                $resultado = NaoTecidosDefinicao::first();
                break;
            case "historia":
                $titulo = Lang::get('frontend.naotecidos.titulo-historia');
                $resultado = NaoTecidosHistoria::first();
                break;
            case "materias-primas":
                $titulo = Lang::get('frontend.naotecidos.titulo-mat-primas');
                $resultado = NaoTecidosMatPrimas::first();
                break;
            case "como-sao-fabricados":
                $titulo = Lang::get('frontend.naotecidos.titulo-fabricacao');
                $resultado = NaoTecidosFabricacao::first();
                break;
            case "transf-acabamento-conversao":
                $titulo = Lang::get('frontend.naotecidos.titulo-transformacao');
                $resultado = NaoTecidosTransformacao::first();
                break;
            case "classificacao":
                $titulo = Lang::get('frontend.naotecidos.titulo-classificacao');
                $resultado = NaoTecidosClassificacao::first();
                break;
            case "identificacao":
                $titulo = Lang::get('frontend.naotecidos.titulo-identificacao');
                $resultado = NaoTecidosIdentificacao::first();
                break;
            case "algumas-aplicacoes":
                $titulo = Lang::get('frontend.naotecidos.titulo-aplicacoes');
                $resultado = NaoTecidosAlgumasAplicacoes::ordenados()->get();
                break;
            case "aplicacoes-a-z":
                $titulo = Lang::get('frontend.naotecidos.titulo-aplicacoes-a-z');
                $resultado = NaoTecidosAplicacoes::orderBy('titulo', 'ASC')->get();
                break;
        }

        $letras = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","X","W","Y","Z"];

        return response()->json(['submenu' => $submenu, 'resultado' => $resultado, 'titulo' => $titulo, 'letras' => $letras]);
    }

    public function getAplicacaoLetra($letra)
    {
        $resultados = NaoTecidosAplicacoes::where('titulo', 'LIKE', $letra . '%')->orderBy('titulo', 'ASC')->get();

        return response()->json(['resultados' => $resultados, 'letra' => $letra]);
    }
}
