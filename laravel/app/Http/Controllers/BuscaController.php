<?php

namespace App\Http\Controllers;

use App\Models\Associado;
use App\Models\Comite;
use App\Models\Diretoria;
use App\Models\Evento;
use App\Models\Grupo;
use App\Models\Institucional;
use App\Models\Link;
use App\Models\NaoTecidosAlgumasAplicacoes;
use App\Models\NaoTecidosAplicacoes;
use App\Models\NaoTecidosClassificacao;
use App\Models\NaoTecidosDefinicao;
use App\Models\NaoTecidosFabricacao;
use App\Models\NaoTecidosHistoria;
use App\Models\NaoTecidosIdentificacao;
use App\Models\NaoTecidosMatPrimas;
use App\Models\NaoTecidosTransformacao;
use App\Models\NormaEnsaio;
use App\Models\Novidade;
use App\Models\Objetivo;
use App\Models\Outro;
use App\Models\Publicacao;
use App\Models\TexteisTecAplicacoes;
use App\Models\TexteisTecClassificacao;
use App\Models\TexteisTecDefinicao;
use App\Models\TexteisTecDesempenho;
use App\Models\TexteisTecFabricacao;
use App\Models\TexteisTecIdentificacao;
use App\Models\TexteisTecMatPrimas;
use App\Models\TexteisTecProcessos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Session;

class BuscaController extends Controller
{
    public function post(Request $request)
    {
        $termo = $request->busca;
        $resultados = [];

        if (!empty($termo)) {
            $busca_institucional = Institucional::busca($termo)->get();
            foreach ($busca_institucional as $dados) {
                $resultados['a-abint'][$dados->id]['link']   = route('a-abint');
                $resultados['a-abint'][$dados->id]['secao']  = Lang::get('frontend.abint.titulo');
                $resultados['a-abint'][$dados->id]['titulo'] = Lang::get('frontend.abint.titulo-busca');
                $resultados['a-abint'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
            }

            $busca_objetivos = Objetivo::busca($termo)->get();
            foreach ($busca_objetivos as $dados) {
                $resultados['objetivos'][$dados->id]['link']   = route('home') . '#' . $dados->id;
                $resultados['objetivos'][$dados->id]['secao']  = Lang::get('frontend.home.titulo-objetivos');
                $resultados['objetivos'][$dados->id]['titulo'] = $dados->{trans('banco.titulo')};
                $resultados['objetivos'][$dados->id]['texto']  = Lang::get('frontend.home.busca-objetivos');
            }

            $busca_diretoria = Diretoria::busca($termo)->get();
            foreach ($busca_diretoria as $dados) {
                $resultados['diretoria'][$dados->id]['link']   = route('a-abint') . '#' . $dados->id;
                $resultados['diretoria'][$dados->id]['secao']  = Lang::get('frontend.abint.diretoria');
                $resultados['diretoria'][$dados->id]['titulo'] = $dados->nome;
                $resultados['diretoria'][$dados->id]['texto']  = Lang::get('frontend.abint.empresa') . $dados->empresa . Lang::get('frontend.abint.busca-abint');
            }

            $busca_associados = Associado::busca($termo)->get();
            foreach ($busca_associados as $dados) {
                $resultados['associados'][$dados->id]['link']   = route('associados') . '#' . $dados->id;
                $resultados['associados'][$dados->id]['secao']  = Lang::get('frontend.associados.titulo');
                $resultados['associados'][$dados->id]['titulo'] = $dados->nome;

                $grupo = explode('|', $dados->grupo);
                $gruposPtEn = [];
                foreach ($grupo as $nome) {
                    $tituloGrupo = Grupo::where('titulo', $nome)->pluck('titulo', 'titulo_en');
                    if ($tituloGrupo) {
                        foreach ($tituloGrupo as $titulo_en => $titulo_pt) {
                            if (Session::get('locale') == "pt") {
                                $gruposPtEn[] = $titulo_pt;
                            }
                            if (Session::get('locale') == "en") {
                                $gruposPtEn[] = $titulo_en;
                            }
                        }
                    }
                }
                $grupos = implode(', ', $gruposPtEn);

                $resultados['associados'][$dados->id]['texto']  = $grupos . Lang::get('frontend.associados.busca');;
            }

            $busca_grupos = Grupo::busca($termo)->get();
            foreach ($busca_grupos as $dados) {
                $resultados['grupos'][$dados->id]['link']   = route('associados') . "#grupo-" . $dados->id;
                $resultados['grupos'][$dados->id]['secao']  = Lang::get('frontend.associados.titulo');
                $resultados['grupos'][$dados->id]['titulo'] = $dados->{trans('banco.titulo')};
                $resultados['grupos'][$dados->id]['texto']  = Lang::get('frontend.associados.busca-grupo');
            }

            $busca_comites = Comite::busca($termo)->get();
            foreach ($busca_comites as $dados) {
                $resultados['comites'][$dados->id]['link']   = route('a-abint') . '#' . $dados->id;
                $resultados['comites'][$dados->id]['secao']  = Lang::get('frontend.abint.titulo-comites');
                $resultados['comites'][$dados->id]['titulo'] = $dados->{trans('banco.nome')};
                $resultados['comites'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
            }

            $busca_publicacoes = Publicacao::busca($termo)->get();
            foreach ($busca_publicacoes as $dados) {
                $resultados['informacao'][$dados->id]['link']   = route('informacao.index', 'publicacoes');
                $resultados['informacao'][$dados->id]['secao']  = Lang::get('frontend.header.publicacoes');
                $resultados['informacao'][$dados->id]['titulo'] = $dados->{trans('banco.titulo')};
                $resultados['informacao'][$dados->id]['texto']  = $dados->{trans('banco.descricao')};
                $resultados['informacao'][$dados->id]['storageId'] = 'publicacoes';
            }

            $busca_eventos = Evento::busca($termo)->get();
            foreach ($busca_eventos as $dados) {
                $resultados['informacao'][$dados->id]['link']   = route('informacao.index', 'cursos-e-eventos');
                $resultados['informacao'][$dados->id]['secao']  = Lang::get('frontend.header.cursos-eventos');
                $resultados['informacao'][$dados->id]['titulo'] = $dados->{trans('banco.nome')};
                $resultados['informacao'][$dados->id]['texto']  = $dados->{trans('banco.descricao')};
                $resultados['informacao'][$dados->id]['storageId'] = 'cursosEEventos';
            }
            
            $busca_normas = NormaEnsaio::busca($termo)->get();
            foreach ($busca_normas as $dados) {
                $resultados['informacao'][$dados->id]['link']   = route('informacao.index', 'normas-e-ensaios');
                $resultados['informacao'][$dados->id]['secao']  = Lang::get('frontend.header.normas-ensaios');
                $resultados['informacao'][$dados->id]['titulo'] = $dados->{trans('banco.titulo')};
                $resultados['informacao'][$dados->id]['texto']  = $dados->{trans('banco.descricao')};
                $resultados['informacao'][$dados->id]['storageId'] = 'normasEEnsaios';
            }

            $busca_novidades = Novidade::busca($termo)->get();
            foreach ($busca_novidades as $dados) {
                $resultados['informacao'][$dados->id]['link']   = route('novidades.show', $dados->slug);
                $resultados['informacao'][$dados->id]['secao']  = Lang::get('frontend.header.imprensa');
                $resultados['informacao'][$dados->id]['titulo'] = $dados->{trans('banco.titulo')};
                $resultados['informacao'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['informacao'][$dados->id]['storageId'] = 'impresa';
            }

            $busca_outros = Outro::busca($termo)->get();
            foreach ($busca_outros as $dados) {
                $resultados['informacao'][$dados->id]['link']   = route('outros.show', $dados->slug);
                $resultados['informacao'][$dados->id]['secao']  = Lang::get('frontend.header.outros');
                $resultados['informacao'][$dados->id]['titulo'] = $dados->{trans('banco.titulo')};
                $resultados['informacao'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['informacao'][$dados->id]['storageId'] = 'outros';
            }

            $busca_oquesaoNT = NaoTecidosDefinicao::busca($termo)->get();
            foreach ($busca_oquesaoNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'o-que-sao');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.o-que-sao');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'oQueSaoNT';
            }
            
            $busca_historiaNT = NaoTecidosHistoria::busca($termo)->get();
            foreach ($busca_historiaNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'historia');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.historia');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'historiaNT';
            }

            $busca_matprimaNT = NaoTecidosMatPrimas::busca($termo)->get();
            foreach ($busca_matprimaNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'materias-primas');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.mat-primas');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'matPrimaNT';
            }

            $busca_comosaofabNT = NaoTecidosFabricacao::busca($termo)->get();
            foreach ($busca_comosaofabNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'como-sao-fabricados');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.fabricacao');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'comoSaoFabNT';
            }

            $busca_transfacabNT = NaoTecidosTransformacao::busca($termo)->get();
            foreach ($busca_transfacabNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'transf-acabamento-conversao');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.transformacao');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'transAcabConv';
            }

            $busca_classifNT = NaoTecidosClassificacao::busca($termo)->get();
            foreach ($busca_classifNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'classificacao');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.classificacao');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'classificacaoNT';
            }

            $busca_identifNT = NaoTecidosIdentificacao::busca($termo)->get();
            foreach ($busca_identifNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'identificacao');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.identificacao');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'idenfiticacaoNT';
            }

            $busca_aplicacoesNT = NaoTecidosAlgumasAplicacoes::busca($termo)->get();
            foreach ($busca_aplicacoesNT as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'algumas-aplicacoes');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.algumas-aplicacoes');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.titulo')} ." - ". $dados->{trans('banco.texto')};
                $resultados['naotecidos'][$dados->id]['storageId']   = 'algumasAplicacoesNT';
            }

            $busca_aplicacoesAZ = NaoTecidosAplicacoes::busca($termo)->get();
            foreach ($busca_aplicacoesAZ as $dados) {
                $resultados['naotecidos'][$dados->id]['link']   = route('naotecidos.index', 'aplicacoes-a-z');
                $resultados['naotecidos'][$dados->id]['secao']  = Lang::get('frontend.naotecidos.titulo');
                $resultados['naotecidos'][$dados->id]['titulo'] = Lang::get('frontend.naotecidos.aplicacoes-a-z');
                $resultados['naotecidos'][$dados->id]['texto']  = $dados->{trans('banco.titulo')} ." - ". $dados->{trans('banco.itens')};
                $resultados['naotecidos'][$dados->id]['storageId'] = 'aplicacoesAZ';
            }

            $busca_oquesaoTT = TexteisTecDefinicao::busca($termo)->get();
            foreach ($busca_oquesaoTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'o-que-sao');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.o-que-sao');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'oQueSaoTT';
            }

            $busca_matprimaTT = TexteisTecMatPrimas::busca($termo)->get();
            foreach ($busca_matprimaTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'materias-primas');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.mat-primas');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'matPrimaTT';
            }

            $busca_comosaofabTT = TexteisTecFabricacao::busca($termo)->get();
            foreach ($busca_comosaofabTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'como-sao-fabricados');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.fabricacao');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'comoSaoFabTT';
            }

            $busca_processosTT = TexteisTecProcessos::busca($termo)->get();
            foreach ($busca_processosTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'processo-transf-acabamento');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.processos');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'processoTransfAcab';
            }

            $busca_classifTT = TexteisTecClassificacao::busca($termo)->get();
            foreach ($busca_classifTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'classificacao');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.classificacao');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'classificacaoTT';
            }

            $busca_desempenhoTT = TexteisTecDesempenho::busca($termo)->get();
            foreach ($busca_desempenhoTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'desempenho');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.desempenho');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'desempenhoTT';
            }

            $busca_identifTT = TexteisTecIdentificacao::busca($termo)->get();
            foreach ($busca_identifTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'identificacao');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.identificacao');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'identificacaoTT';
            }

            $busca_aplicacoesTT = TexteisTecAplicacoes::busca($termo)->get();
            foreach ($busca_aplicacoesTT as $dados) {
                $resultados['texteis-tecnicos'][$dados->id]['link']   = route('texteis-tecnicos.index', 'algumas-aplicacoes');
                $resultados['texteis-tecnicos'][$dados->id]['secao']  = Lang::get('frontend.texteis-tec.titulo');
                $resultados['texteis-tecnicos'][$dados->id]['titulo'] = Lang::get('frontend.texteis-tec.aplicacoes');
                $resultados['texteis-tecnicos'][$dados->id]['texto']  = $dados->{trans('banco.texto')};
                $resultados['texteis-tecnicos'][$dados->id]['storageId'] = 'algumasAplicacoesTT';
            }
        } else {
            $resultados = [];
        }

        return view('frontend.busca', compact('resultados', 'termo'));
    }
}
