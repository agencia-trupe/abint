<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContatosAssocieSeRequest;
use App\Models\AssocieSe;
use App\Models\Comite;
use App\Models\Contato;
use App\Models\ContatoAssocieSe;
use Illuminate\Support\Facades\Mail;

class AssocieSeController extends Controller
{
    public function index()
    {
        $associe = AssocieSe::first();
        $comites = Comite::ordenados()->get();

        return view('frontend.associe-se', compact('associe', 'comites'));
    }

    public function post(ContatosAssocieSeRequest $request, ContatoAssocieSe $contato)
    {
        $data = $request->all();

        $contato->create($data);
        $this->sendMail($data);

        return redirect('associe-se')->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (!$email = Contato::first()->email) {
            return false;
        }

        Mail::send('emails.associe-se', $data, function ($m) use ($email, $data) {
            $m->to($email, config('app.name'))
                ->subject('[ASSOCIE-SE] ' . config('app.name'))
                ->replyTo($data['email'], $data['nome']);
        });
    }
}
