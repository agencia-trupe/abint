<?php

namespace App\Http\Controllers\Painel\NaoTecidos;

use App\Http\Controllers\Controller;
use App\Http\Requests\NaoTecidosAlgumasAplicacoesRequest;
use App\Models\NaoTecidosAlgumasAplicacoes;

class NaoTecidosAlgumasAplicacoesController extends Controller
{
    public function index()
    {
        $registros = NaoTecidosAlgumasAplicacoes::ordenados()->get();

        return view('painel.naotecidos.algumas-aplicacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.naotecidos.algumas-aplicacoes.create');
    }

    public function store(NaoTecidosAlgumasAplicacoesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = NaoTecidosAlgumasAplicacoes::upload_imagem();

            NaoTecidosAlgumasAplicacoes::create($input);

            return redirect()->route('painel.naotecidos.algumas-aplicacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($registro)
    {
        $registro = NaoTecidosAlgumasAplicacoes::find($registro);

        return view('painel.naotecidos.algumas-aplicacoes.edit', compact('registro'));
    }

    public function update(NaoTecidosAlgumasAplicacoesRequest $request, $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = NaoTecidosAlgumasAplicacoes::upload_imagem();

            $registro = NaoTecidosAlgumasAplicacoes::find($registro);

            $registro->update($input);

            return redirect()->route('painel.naotecidos.algumas-aplicacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($registro)
    {
        try {
            $registro = NaoTecidosAlgumasAplicacoes::find($registro);

            $registro->delete();

            return redirect()->route('painel.naotecidos.algumas-aplicacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
