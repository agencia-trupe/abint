<?php

namespace App\Http\Controllers\Painel\NaoTecidos;

use App\Http\Controllers\Controller;
use App\Http\Requests\NaoTecidosGeralRequest;
use App\Models\NaoTecidosMatPrimas;

class NaoTecidosMatPrimasController extends Controller
{
    public function index()
    {
        $registro = NaoTecidosMatPrimas::first();

        return view('painel.naotecidos.materias-primas.edit', compact('registro'));
    }

    public function update(NaoTecidosGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = NaoTecidosMatPrimas::where('id', $registro)->update($input);

            return redirect()->route('painel.naotecidos.materias-primas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
