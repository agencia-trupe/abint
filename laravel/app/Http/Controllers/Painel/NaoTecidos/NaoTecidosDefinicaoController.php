<?php

namespace App\Http\Controllers\Painel\NaoTecidos;

use App\Http\Controllers\Controller;
use App\Http\Requests\NaoTecidosGeralRequest;
use App\Models\NaoTecidosDefinicao;

class NaoTecidosDefinicaoController extends Controller
{
    public function index()
    {
        $registro = NaoTecidosDefinicao::first();

        return view('painel.naotecidos.o-que-sao.edit', compact('registro'));
    }

    public function update(NaoTecidosGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = NaoTecidosDefinicao::where('id', $registro)->update($input);

            return redirect()->route('painel.naotecidos.o-que-sao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
