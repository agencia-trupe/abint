<?php

namespace App\Http\Controllers\Painel\NaoTecidos;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NaoTecidosGeralRequest;
use App\Models\NaoTecidosTransformacao;

class NaoTecidosTransformacaoController extends Controller
{
    public function index()
    {
        $registro = NaoTecidosTransformacao::first();

        return view('painel.naotecidos.transf-acabamento-conversao.edit', compact('registro'));
    }

    public function update(NaoTecidosGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = NaoTecidosTransformacao::where('id', $registro)->update($input);

            return redirect()->route('painel.naotecidos.transf-acabamento-conversao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
