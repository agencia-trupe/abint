<?php

namespace App\Http\Controllers\Painel\NaoTecidos;

use App\Http\Controllers\Controller;
use App\Http\Requests\NaoTecidosAplicacoesRequest;
use App\Models\NaoTecidosAplicacoes;

class NaoTecidosAplicacoesController extends Controller
{
    public function index()
    {
        $registros = NaoTecidosAplicacoes::orderBy('titulo', 'ASC')->paginate(15);

        return view('painel.naotecidos.aplicacoes-a-z.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.naotecidos.aplicacoes-a-z.create');
    }

    public function store(NaoTecidosAplicacoesRequest $request)
    {
        try {
            $input = $request->all();

            NaoTecidosAplicacoes::create($input);

            return redirect()->route('painel.naotecidos.aplicacoes-a-z.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($registro)
    {
        $registro = NaoTecidosAplicacoes::find($registro);

        return view('painel.naotecidos.aplicacoes-a-z.edit', compact('registro'));
    }

    public function update(NaoTecidosAplicacoesRequest $request, $registro)
    {
        try {
            $input = $request->all();

            $registro = NaoTecidosAplicacoes::find($registro);

            $registro->update($input);

            return redirect()->route('painel.naotecidos.aplicacoes-a-z.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($registro)
    {
        try {
            $registro = NaoTecidosAplicacoes::find($registro);

            $registro->delete();

            return redirect()->route('painel.naotecidos.aplicacoes-a-z.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
