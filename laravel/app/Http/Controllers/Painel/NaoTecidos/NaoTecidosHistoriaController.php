<?php

namespace App\Http\Controllers\Painel\NaoTecidos;

use App\Http\Controllers\Controller;
use App\Http\Requests\NaoTecidosGeralRequest;
use App\Models\NaoTecidosHistoria;

class NaoTecidosHistoriaController extends Controller
{
    public function index()
    {
        $registro = NaoTecidosHistoria::first();

        return view('painel.naotecidos.historia.edit', compact('registro'));
    }

    public function update(NaoTecidosGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = NaoTecidosHistoria::where('id', $registro)->update($input);

            return redirect()->route('painel.naotecidos.historia.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
