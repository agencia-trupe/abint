<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssociadosRequest;
use App\Models\Associado;
use App\Models\Grupo;

class AssociadosController extends Controller
{
    public function index()
    {
        $registros = Associado::orderBy('nome', 'ASC')->get();

        return view('painel.associados.index', compact('registros'));
    }

    public function create()
    {
        $grupos = Grupo::pluck('titulo', 'id');
        
        $associadoGrupos = [];

        return view('painel.associados.create', compact('grupos', 'associadoGrupos'));
    }

    public function store(AssociadosRequest $request)
    {
        try {
            $input = $request->all();
            $input['grupo'] = implode('|', $request->grupo);

            if (isset($input['imagem'])) $input['imagem'] = Associado::upload_imagem();

            Associado::create($input);

            return redirect()->route('painel.associados.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Associado $registro)
    {
        $grupos = Grupo::pluck('titulo', 'id');

        $associadoGrupos = explode('|', $registro->grupo);

        return view('painel.associados.edit', compact('registro', 'grupos', 'associadoGrupos'));
    }

    public function update(AssociadosRequest $request, Associado $registro)
    {
        try {
            $input = $request->all();
            $input['grupo'] = implode('|', $request->grupo);

            if (isset($input['imagem'])) $input['imagem'] = Associado::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.associados.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Associado $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.associados.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
