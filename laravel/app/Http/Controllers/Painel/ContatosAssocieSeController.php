<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ContatoAssocieSe;

class ContatosAssocieSeController extends Controller
{
    public function index()
    {
        $contatos = ContatoAssocieSe::orderBy('created_at', 'DESC')->get();

        return view('painel.associe-se.contatos.index', compact('contatos'));
    }

    public function show($contato_id)
    {
        $contato = ContatoAssocieSe::where('id', $contato_id)->first();
        $contato->update(['lido' => 1]);

        return view('painel.associe-se.contatos.show', compact('contato'));
    }

    public function destroy($contato_id)
    {
        try {
            $contato = ContatoAssocieSe::where('id', $contato_id)->first();
            $contato->delete();

            return redirect()->route('painel.associe-se.contatos.index')->with('success', 'Mensagem excluída com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: ' . $e->getMessage()]);
        }
    }

    public function toggle($contato_id, Request $request)
    {
        try {
            $contato = ContatoAssocieSe::where('id', $contato_id)->first();
            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.associe-se.contatos.index')->with('success', 'Mensagem alterada com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: ' . $e->getMessage()]);
        }
    }
}
