<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ObjetivosRequest;
use App\Models\Objetivo;

class ObjetivosController extends Controller
{
    public function index()
    {
        $registros = Objetivo::ordenados()->get();

        return view('painel.objetivos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.objetivos.create');
    }

    public function store(ObjetivosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Objetivo::upload_imagem();

            Objetivo::create($input);

            return redirect()->route('painel.objetivos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Objetivo $registro)
    {
        return view('painel.objetivos.edit', compact('registro'));
    }

    public function update(ObjetivosRequest $request, Objetivo $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Objetivo::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.objetivos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Objetivo $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.objetivos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
