<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\DiretoriaRequest;
use App\Models\Cargo;
use App\Models\Diretoria;

class DiretoriaController extends Controller
{
    public function index()
    {
        $registros = Diretoria::ordenados()
            ->join('cargos', 'cargos.id', '=', 'diretoria.cargo_id')
            ->select('cargos.titulo as cargo_nome', 'cargos.id as cargo_id', 'diretoria.id as id', 'diretoria.nome as nome', 'diretoria.empresa as empresa')
            ->get();

        return view('painel.diretoria.index', compact('registros'));
    }

    public function create()
    {
        $cargos = Cargo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.diretoria.create', compact('cargos'));
    }

    public function store(DiretoriaRequest $request)
    {
        try {
            $input = $request->all();

            Diretoria::create($input);

            return redirect()->route('painel.diretoria.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Diretoria $registro)
    {
        $cargos = Cargo::orderBy('titulo', 'asc')->pluck('titulo', 'id');

        return view('painel.diretoria.edit', compact('registro', 'cargos'));
    }

    public function update(DiretoriaRequest $request, Diretoria $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.diretoria.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Diretoria $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.diretoria.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
