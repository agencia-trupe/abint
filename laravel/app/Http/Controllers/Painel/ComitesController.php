<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ComitesRequest;
use App\Models\Comite;

class ComitesController extends Controller
{
    public function index()
    {
        $registros = Comite::ordenados()->get();

        return view('painel.comites.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.comites.create');
    }

    public function store(ComitesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Comite::upload_imagem();

            Comite::create($input);

            return redirect()->route('painel.comites.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Comite $registro)
    {
        return view('painel.comites.edit', compact('registro'));
    }

    public function update(ComitesRequest $request, Comite $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Comite::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.comites.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Comite $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.comites.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
