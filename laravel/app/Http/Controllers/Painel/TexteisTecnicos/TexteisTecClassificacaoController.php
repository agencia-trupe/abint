<?php

namespace App\Http\Controllers\Painel\TexteisTecnicos;

use App\Http\Controllers\Controller;
use App\Http\Requests\TexteisTecGeralRequest;
use App\Models\TexteisTecClassificacao;

class TexteisTecClassificacaoController extends Controller
{
    public function index()
    {
        $registro = TexteisTecClassificacao::first();

        return view('painel.texteis-tecnicos.classificacao.edit', compact('registro'));
    }

    public function update(TexteisTecGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = TexteisTecClassificacao::where('id', $registro)->update($input);

            return redirect()->route('painel.texteis-tecnicos.classificacao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
