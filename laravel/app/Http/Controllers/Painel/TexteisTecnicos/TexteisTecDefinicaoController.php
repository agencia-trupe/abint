<?php

namespace App\Http\Controllers\Painel\TexteisTecnicos;

use App\Http\Controllers\Controller;
use App\Http\Requests\TexteisTecGeralRequest;
use App\Models\TexteisTecDefinicao;

class TexteisTecDefinicaoController extends Controller
{
    public function index()
    {
        $registro = TexteisTecDefinicao::first();

        return view('painel.texteis-tecnicos.o-que-sao.edit', compact('registro'));
    }

    public function update(TexteisTecGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = TexteisTecDefinicao::where('id', $registro)->update($input);

            return redirect()->route('painel.texteis-tecnicos.o-que-sao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
