<?php

namespace App\Http\Controllers\Painel\TexteisTecnicos;

use App\Http\Controllers\Controller;
use App\Http\Requests\TexteisTecGeralRequest;
use App\Models\TexteisTecMatPrimas;

class TexteisTecMatPrimasController extends Controller
{
    public function index()
    {
        $registro = TexteisTecMatPrimas::first();

        return view('painel.texteis-tecnicos.materias-primas.edit', compact('registro'));
    }

    public function update(TexteisTecGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = TexteisTecMatPrimas::where('id', $registro)->update($input);

            return redirect()->route('painel.texteis-tecnicos.materias-primas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
