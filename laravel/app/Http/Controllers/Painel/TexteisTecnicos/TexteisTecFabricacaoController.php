<?php

namespace App\Http\Controllers\Painel\TexteisTecnicos;

use App\Http\Controllers\Controller;
use App\Http\Requests\TexteisTecGeralRequest;
use App\Models\TexteisTecFabricacao;

class TexteisTecFabricacaoController extends Controller
{
    public function index()
    {
        $registro = TexteisTecFabricacao::first();

        return view('painel.texteis-tecnicos.como-sao-fabricados.edit', compact('registro'));
    }

    public function update(TexteisTecGeralRequest $request, $registro)
    {
        try {
            $input['texto'] = $request->texto;
            $input['texto_en'] = $request->texto_en;

            $registro = TexteisTecFabricacao::where('id', $registro)->update($input);

            return redirect()->route('painel.texteis-tecnicos.como-sao-fabricados.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
