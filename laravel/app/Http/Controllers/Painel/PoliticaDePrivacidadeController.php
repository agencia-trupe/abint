<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\PoliticaDePrivacidadeRequest;
use App\Models\PoliticaDePrivacidade;

class PoliticaDePrivacidadeController extends Controller
{
    public function index()
    {
        $politica = PoliticaDePrivacidade::first();

        return view('painel.politica-de-privacidade.edit', compact('politica'));
    }

    public function update(PoliticaDePrivacidadeRequest $request, PoliticaDePrivacidade $politica)
    {
        try {
            $input = $request->all();

            $politica->update($input);

            return redirect()->route('painel.politica-de-privacidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
