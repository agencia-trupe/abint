<?php

namespace App\Http\Controllers\Painel\Informacao;

use App\Http\Controllers\Controller;
use App\Http\Requests\NovidadesRequest;
use App\Models\Novidade;

class NovidadesController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'Ñ' => 'N'
        );

        return $conversao;
    }

    public function index()
    {
        $novidades = Novidade::ordenados()->get();

        return view('painel.informacao.novidades.index', compact('novidades'));
    }

    public function create()
    {
        return view('painel.informacao.novidades.create');
    }

    public function store(NovidadesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Novidade::upload_imagem();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/pdf';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            Novidade::create($input);

            return redirect()->route('painel.informacao.novidades.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Novidade $novidade)
    {
        return view('painel.informacao.novidades.edit', compact('novidade'));
    }

    public function update(NovidadesRequest $request, Novidade $novidade)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Novidade::upload_imagem();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/pdf';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $novidade->update($input);

            return redirect()->route('painel.informacao.novidades.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Novidade $novidade)
    {
        try {
            $novidade->delete();

            return redirect()->route('painel.informacao.novidades.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
