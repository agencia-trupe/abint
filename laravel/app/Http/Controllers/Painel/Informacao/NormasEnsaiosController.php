<?php

namespace App\Http\Controllers\Painel\Informacao;

use App\Http\Controllers\Controller;
use App\Http\Requests\NormasEnsaiosRequest;
use App\Models\NormaEnsaio;

class NormasEnsaiosController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'Ñ' => 'N'
        );

        return $conversao;
    }

    public function index()
    {
        $registros = NormaEnsaio::ordenados()->get();

        return view('painel.informacao.normas-e-ensaios.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.informacao.normas-e-ensaios.create');
    }

    public function store(NormasEnsaiosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = NormaEnsaio::upload_imagem();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/pdf';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            NormaEnsaio::create($input);

            return redirect()->route('painel.informacao.normas-e-ensaios.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($registro_id)
    {
        $registro = NormaEnsaio::where('id', $registro_id)->first();

        return view('painel.informacao.normas-e-ensaios.edit', compact('registro'));
    }

    public function update(NormasEnsaiosRequest $request, $registro_id)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = NormaEnsaio::upload_imagem();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/pdf';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $registro = NormaEnsaio::where('id', $registro_id)->first();
            $registro->update($input);

            return redirect()->route('painel.informacao.normas-e-ensaios.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($registro_id)
    {
        try {
            $registro = NormaEnsaio::where('id', $registro_id)->first();
            $registro->delete();

            return redirect()->route('painel.informacao.normas-e-ensaios.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
