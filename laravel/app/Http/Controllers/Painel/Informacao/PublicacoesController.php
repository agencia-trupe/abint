<?php

namespace App\Http\Controllers\Painel\Informacao;

use App\Http\Controllers\Controller;
use App\Http\Requests\PublicacoesRequest;
use App\Models\Publicacao;

class PublicacoesController extends Controller
{
    public function removerAcentos()
    {
        $conversao = array(
            'á' => 'a', 'à' => 'a', 'ã' => 'a', 'â' => 'a', 'é' => 'e',
            'ê' => 'e', 'í' => 'i', 'ï' => 'i', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', "ö" => "o",
            'ú' => 'u', 'ü' => 'u', 'ç' => 'c', 'ñ' => 'n', 'Á' => 'A', 'À' => 'A', 'Ã' => 'A',
            'Â' => 'A', 'É' => 'E', 'Ê' => 'E', 'Í' => 'I', 'Ï' => 'I', "Ö" => "O", 'Ó' => 'O',
            'Ô' => 'O', 'Õ' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'Ç' => 'C', 'Ñ' => 'N'
        );

        return $conversao;
    }

    public function index()
    {
        $registros = Publicacao::ordenados()->get();

        return view('painel.informacao.publicacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.informacao.publicacoes.create');
    }

    public function store(PublicacoesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Publicacao::upload_imagem();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/pdf';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            Publicacao::create($input);

            return redirect()->route('painel.informacao.publicacoes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Publicacao $registro)
    {
        return view('painel.informacao.publicacoes.edit', compact('registro'));
    }

    public function update(PublicacoesRequest $request, Publicacao $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Publicacao::upload_imagem();

            $file = $request->file('arquivo');

            if ($request->hasFile('arquivo')) {
                if ($request->file('arquivo')->isValid()) {
                    $filename = strtoupper(str_replace(' ', '-', strtr($file->getClientOriginalName(), $this->removerAcentos())));
                    $path = public_path() . '/assets/pdf';
                    $file->move($path, $filename);
                    $input['arquivo'] = $filename;
                }
            }

            $registro->update($input);

            return redirect()->route('painel.informacao.publicacoes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Publicacao $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.informacao.publicacoes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
