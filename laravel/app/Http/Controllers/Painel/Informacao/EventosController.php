<?php

namespace App\Http\Controllers\Painel\Informacao;

use App\Http\Controllers\Controller;
use App\Http\Requests\EventosRequest;
use App\Models\Configuracoes;
use App\Models\Evento;

class EventosController extends Controller
{
    public function index()
    {
        $registros = Evento::ordenados()->get();

        return view('painel.informacao.cursos-e-eventos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.informacao.cursos-e-eventos.create');
    }

    public function store(EventosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Evento::upload_imagem();

            Evento::create($input);

            return redirect()->route('painel.informacao.cursos-e-eventos.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit($registro_id)
    {
        $registro = Evento::where('id', $registro_id)->first();

        return view('painel.informacao.cursos-e-eventos.edit', compact('registro'));
    }

    public function update(EventosRequest $request, $registro_id)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Evento::upload_imagem();

            $registro = Evento::where('id', $registro_id)->first();

            $registro->update($input);

            return redirect()->route('painel.informacao.cursos-e-eventos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy($registro_id)
    {
        try {
            $registro = Evento::where('id', $registro_id)->first();
            $registro->delete();

            return redirect()->route('painel.informacao.cursos-e-eventos.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
