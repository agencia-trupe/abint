<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Associado;
use App\Models\Comite;
use App\Models\ComiteAssociado;

class ComitesAssociadosController extends Controller
{
    public function index(Comite $registro)
    {
        $membros = ComiteAssociado::comite($registro->id)
            ->join('comites', 'comites.id', '=', 'comites_associados.comite_id')
            ->join('associados', 'associados.id', '=', 'comites_associados.associado_id')
            ->select('comites.id as comite_id', 'comites.nome as comite_nome', 'associados.id as associado_id', 'associados.nome as associado_nome', 'associados.imagem as associado_imagem', 'comites_associados.ordem as ordem', 'comites_associados.id as id')
            ->ordenados()->get();

        return view('painel.comites.associados.index', compact('membros', 'registro'));
    }

    public function create(Comite $registro)
    {
        $associados = Associado::orderBy('nome', 'ASC')->pluck('nome', 'id');

        return view('painel.comites.associados.create', compact('associados', 'registro'));
    }

    public function store(Comite $registro, Request $request)
    {
        try {
            $input = $request->all();
            $input['comite_id'] = $registro->id;

            ComiteAssociado::create($input);

            return redirect()->route('painel.comites.associados.index', $registro)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: ' . $e->getMessage();
        }
    }

    public function destroy(Comite $registro, $membro)
    {
        try {
            $associadoComite = ComiteAssociado::where('id', $membro->id)->where('comite_id', $registro->id)->first();
            
            $associadoComite->delete();

            return redirect()->route('painel.comites.associados.index', $registro)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: ' . $e->getMessage()]);
        }
    }
}
