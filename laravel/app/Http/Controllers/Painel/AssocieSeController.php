<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssocieSeRequest;
use App\Models\AssocieSe;

class AssocieSeController extends Controller
{
    public function index()
    {
        $registro = AssocieSe::first();

        return view('painel.associe-se.edit', compact('registro'));
    }

    public function update(AssocieSeRequest $request, AssocieSe $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.associe-se.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
