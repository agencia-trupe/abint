<?php

namespace App\Http\Controllers;

use App\Models\Associado;
use App\Models\Grupo;

class AssociadosController extends Controller
{
    public function index()
    {
        $associados = Associado::select('id', 'imagem', 'nome', 'link', 'grupo')->orderBy('nome', 'ASC')->get();
        $grupos = Grupo::ordenados()->get();

        return view('frontend.associados', compact('associados', 'grupos'));
    }
}
