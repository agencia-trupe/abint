<?php

namespace App\Http\Controllers;

use App\Models\Comite;
use App\Models\ComiteAssociado;
use App\Models\Diretoria;
use App\Models\Institucional;

class AAbintController extends Controller
{
    public function index()
    {
        $abint = Institucional::first();

        $diretoria = Diretoria::join('cargos', 'cargos.id', '=', 'diretoria.cargo_id')
                                ->select('cargos.id as cargo_id', 'cargos.titulo as titulo', 'diretoria.id as id', 'diretoria.nome as nome', 'diretoria.empresa as empresa')
                                ->ordenados()->get();

        $presidente = $diretoria->where('titulo', 'Presidente')->first();
        $vicePresidente = $diretoria->where('titulo', 'Vice-Presidente')->first();
        $diretores = $diretoria->where('titulo', 'Diretor')->all();
        $conselheiros = $diretoria->where('titulo', 'Conselheiro')->all();

        $comites = Comite::ordenados()->get();
        $associados = ComiteAssociado::join('associados', 'associados.id', '=', 'comites_associados.associado_id')
                                    ->select('associados.id as associado_id', 'associados.imagem as imagem', 'associados.nome as nome', 'associados.link as link', 'comites_associados.id as id', 'comites_associados.ordem as ordem', 'comites_associados.comite_id as comite_id')
                                    ->ordenados()
                                    ->get();

        return view('frontend.a-abint', compact('abint', 'presidente', 'vicePresidente', 'diretores', 'conselheiros', 'comites', 'associados'));
    }
}
