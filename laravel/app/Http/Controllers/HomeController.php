<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\Comite;
use App\Models\Destaque;
use App\Models\Evento;
use App\Models\Novidade;
use App\Models\Objetivo;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $destaques = Destaque::ordenados()->get();
        $objetivos = Objetivo::ordenados()->get();
        $comites = Comite::ordenados()->get();
        $novidades = Novidade::orderBy('data', 'DESC')->take(3)->get();
        // $eventos = Evento::orderBy('data', 'ASC')->where('data', '>=', date('Y-m-d'))->take(3)->get();

        return view('frontend.home', compact('banners', 'destaques', 'objetivos', 'comites', 'novidades'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
